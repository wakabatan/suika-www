#!/usr/bin/perl
use strict;

my $log_file_name = 'log/gets.txt';
open STDOUT, '>>', $log_file_name;
open STDERR, '>>', $log_file_name;

while (not -f 'STOP' and not -f 'RESTART') {
  system 'perl', 'bin/get.pl';
  
  sleep 60*60;
}

if (-f 'RESTART') {
  unlink 'RESTART';
  exec 'perl', $0;
}
unlink 'STOP';
