/*
  Open linked document with Live DOM Viewer, for WinIE

    See also: <https://suika.suikawiki.org/gate/2005/sw/Bookmarklet#anchor-53>

    License: Public Domain
*/

window.OpenInLiveDOMViewer = function (uri) {
  var xhr = window.XMLHttpRequest
      ? new XMLHttpRequest ()
      : new ActiveXObject ('Microsoft.XMLHTTP');
  xhr.open ("GET", uri, false);
  xhr.send (null);
  location.href = 
      'http://software.hixie.ch/utilities/js/live-dom-viewer/?'
      + encodeURIComponent (xhr.responseText);
};

var links = [];
var linksLength = document.links.length;
for (var i = 0; i < linksLength; i++) {
  links[i] = document.links[i];
}

for (var i = 0; i < linksLength; i++) {
  var link = links[i];
  var container = document.createDocumentFragment ();
  container.appendChild
      (document.createTextNode (' ['));
  var a = container.appendChild
      (document.createElement ('a'));
  a.href = link.href;
  a.appendChild (document.createTextNode ('Live DOM Viewer'));
  a.onclick = function () {
    window.OpenInLiveDOMViewer (this.href);
    return false;
  };
  container.appendChild
      (document.createTextNode (']'));
  link.parentNode.insertBefore
      (container, link.nextSibling);
}
