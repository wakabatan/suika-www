function JSATimeline () {
  this.currentTime = 0;
} // JSATimeline

// Force the "current time" property of the timeline object being /t/.
JSATimeline.prototype.setCurrentTime = function (t) {
  this.currentTime = t;
}; // setCurrentTime

function JSAEvent (time) {
  this,time = time;
} // JSAEvent

function JSAEventSet () {
  this.events = [];
} // JSAEventSet

JSAEventSet.prototype.addEvent = function (e) {
  if (e.eventSet) {
    throw new JSAEventException ("RegisteredEvent");
  }

  this.events.push (e);
  this.events = this.events.sort (function (a, b) { return a.time < b.time });
}; // addEvent

function JSAEventException (s) {
  this.type = s;
} // JSAEventException

JSAEventException.prototype.toString = function () {
  return "JSAEventException: " + this.type;
}; // toString