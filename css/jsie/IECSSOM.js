
/*

|styleSheet| Object
<http://msdn2.microsoft.com/en-us/library/ms535871(VS.85).aspx>

*/

function IEStyleSheet (href, parentStyleSheet) {
  this.href = href;
  this.parentStyleSheet = parentStyleSheet;
  this.imports = [];
  this.pages = [];
  this.rules = [];
}

// TODO: property canHaveHTML

// property disabled
// property href

// TODO: property id
// TODO: property isContentEditable
// TODO: property isDisabled
// TODO: property isMultiLine
// TODO: property owningElement

// property parentStyleSheet

// TODO: property readOnly

// property title

// TODO: property type

// collection imports
  // TODO: method namedItem
// collection pages
// collection rules

// TODO: method addImport
// TODO: method addPageRule
// TODO: method addRule
// TODO: method fireEvent
// TODO: method removeImport
// TODO: method removeRule

// TODO: property cssText
// TODO: property media
// TODO: property textAutospace

/*

|page| Object
<http://msdn2.microsoft.com/en-us/library/ms535879(VS.85).aspx>

*/

function IEPage (pseudoClass, selector) {
  this.pseudoClass = pseudoClass;
  this.selector = selector;
}

// property pseudoClass
// property selector

/*

|CSSStyleDeclaration| Interface

*/

function IEStyleDeclaration (mediaText) {
  this._MediaText = mediaText;
}

// internal property _MediaText
