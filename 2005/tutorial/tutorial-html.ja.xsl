<?xml version="1.0" encoding="iso-2022-jp"?>
<!DOCTYPE t:stylesheet [
  <!ENTITY sort-key "
    self::doc:*/@key |
                                                       @doc:key |
                                                       self::*[not (self::doc:*) and
                                                               not (@doc:key)] |
                                                       self::doc:*[not (@key)]
  ">
]>
<t:stylesheet xmlns:t="http://www.w3.org/1999/XSL/Transform"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://suika.fam.cx/~wakaba/archive/2005/7/tutorial#"
    xmlns:tree="http://pc5.2ch.net/test/read.cgi/hp/1101043958/564"
    xmlns:xhtml1="http://www.w3.org/1999/xhtml"
    xmlns:xhtml2="http://www.w3.org/2002/06/xhtml2/"
    xmlns:html5="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    version="1.0">
  
  <t:template match="child::doc:document">
    <html lang="{@xml:lang}" xml:lang="{@xml:lang}">
      <head>
        <t:apply-templates select="child::xhtml2:h[position () = 1]" mode="title"/>
        <link rel="stylesheet" href="tutorial-style.css"
            type="text/css" media="all"/>
      </head>
      <body>
        <t:apply-templates select="@*"/>
        <t:apply-templates select="child::*" mode="h1"/>
        <t:if test="descendant::doc:ed | descendant::doc:todo">
          <t:apply-templates select="self::node ()" mode="todo-list"/>
        </t:if>
      </body>
    </html>
  </t:template>
  
  <t:template match="doc:document/@published-year"/>
  
  <t:template match="doc:document" mode="todo-list">
    <div class="section ed">
      <h2>$BL$40@.$N2U=j(B</h2>
      <ol class="xoxo">
        <t:apply-templates select="descendant::doc:ed |
                                   descendant::doc:todo" mode="todo-list"/>
      </ol>
    </div>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'toc']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <ol class="xoxo doc-toc">
        <t:apply-templates select="child::html5:section |
                                   child::doc:appendix |
                                   child::doc:presection" mode="toc"/>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'toc-detail']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <ol class="xoxo doc-toc-detail">
        <t:apply-templates select="child::html5:section |
                                   child::doc:appendix |
                                   child::doc:presection" mode="toc-detail"/>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'list-of-figures']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <ol class="xoxo doc-list-of-figures">
        <t:apply-templates select="descendant::doc:fig |
                                   descendant::html5:table |
                                   descendant::doc:quote |
                                   descendant::doc:example" mode="toc-figures"/>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'list-of-columns']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <ol class="xoxo doc-list-of-columns">
        <t:apply-templates select="descendant::doc:miniColumn" mode="toc-columns"/>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'list-of-keypoints']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <ol class="xoxo doc-list-of-keypoints">
        <t:apply-templates select="descendant::doc:keyPoints" mode="toc-keypoints"/>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'answers']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <t:apply-templates select="child::html5:section" mode="answer-h3"/>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'index-aiueo']" mode="h2">
    <t:for-each select="ancestor::doc:document">
      <t:variable name="target" select="descendant::html5:dfn |
                                        descendant::doc:iw"/>
      <t:variable name="target-a"
          select="$target[contains ('$B$"$$$&$($*%"%$%&%(%*(B',
                                    substring (string (&sort-key;), 1, 1))]"/>
      <ol class="xoxo">
        <t:call-template name="index-part">
          <t:with-param name="header">$B$"9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-a'"/>
          <t:with-param name="items" select="$target-a"/>
        </t:call-template>
        <t:variable name="target-ka"
            select="$target[contains ('$B$+$-$/$1$3%+%-%/%1%3$,$.$0$2$4%,%.%0%2%4(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$+9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-ka'"/>
          <t:with-param name="items" select="$target-ka"/>
        </t:call-template>
        <t:variable name="target-sa"
            select="$target[contains ('$B$5$7$9$;$=%5%7%9%;%=$6$8$:$<$>%6%8%:%<%>(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$59T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-sa'"/>
          <t:with-param name="items" select="$target-sa"/>
        </t:call-template>
        <t:variable name="target-ta"
            select="$target[contains ('$B$?$A$D$F$H%?%A%D%F%H$@$B$E$G$I%@%B%E%G%I(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$?9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="items" select="$target-ta"/>
          <t:with-param name="name" select="'aiueo-ta'"/>
        </t:call-template>
        <t:variable name="target-na"
            select="$target[contains ('$B$J$K$L$M$N%J%K%L%M%N(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$J9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-na'"/>
          <t:with-param name="items" select="$target-na"/>
        </t:call-template>
        <t:variable name="target-ha"
            select="$target[contains ('$B$O$R$U$X$[%O%R%U%X%[$P$S$V$Y$\%P%S%V%Y%\$Q$T$W$Z$]%Q%T%W%Z%](B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$O9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="items" select="$target-ha"/>
          <t:with-param name="name" select="'aiueo-ha'"/>
        </t:call-template>
        <t:variable name="target-ma"
            select="$target[contains ('$B$^$_$`$a$b%^%_%`%a%b(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$^9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-ma'"/>
          <t:with-param name="items" select="$target-ma"/>
        </t:call-template>
        <t:variable name="target-ya"
            select="$target[contains ('$B$d$f$h%d%f%h(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$d9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-ya'"/>
          <t:with-param name="items" select="$target-ya"/>
        </t:call-template>
        <t:variable name="target-ra"
            select="$target[contains ('$B$i$j$k$l$m%i%j%k%l%m(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$i9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-ra'"/>
          <t:with-param name="items" select="$target-ra"/>
        </t:call-template>
        <t:variable name="target-wa"
            select="$target[contains ('$B$o$p$q$r$s%o%p%q%r%s(B',
                                      substring (string (&sort-key;), 1, 1))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$o9T(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-wa'"/>
          <t:with-param name="items" select="$target-wa"/>
        </t:call-template>
        <t:variable name="target-misc"
            select="$target[not (contains ('$B$"$$$&$($*%"%$%&%(%*$+$-$/$1$3%+%-%/%1%3$,$.$0$2$4%,%.%0%2%4$5$7$9$;$=%5%7%9%;%=$6$8$:$<$>%6%8%:%<%>$?$A$D$F$H%?%A%D%F%H$@$B$E$G$I%@%B%E%G%I$J$K$L$M$N%J%K%L%M%N$O$R$U$X$[%O%R%U%X%[$P$S$V$Y$\%P%S%V%Y%\$Q$T$W$Z$]%Q%T%W%Z%]$^$_$`$a$b%^%_%`%a%b$d$f$h%d%f%h$i$j$k$l$m%i%j%k%l%m$o$p$q$r$s%o%p%q%r%s(B',
                                      substring (string (&sort-key;), 1, 1)))]"/>
        <t:call-template name="index-part">
          <t:with-param name="header">$B$=$NB>(B</t:with-param>
          <t:with-param name="header-lang">ja</t:with-param>
          <t:with-param name="name" select="'aiueo-misc'"/>
          <t:with-param name="items" select="$target-misc"/>
        </t:call-template>
      </ol>
    </t:for-each>
  </t:template>
  
  <t:template name="index-part">
    <t:param name="header"/>
    <t:param name="header-lang"/>
    <t:param name="name"/>
    <t:param name="items"/>
    <t:if test="$items">
      <li class="doc-index-{$name}">
        <div class="non-para" lang="{$header-lang}" xml:lang="{$header-lang}">
          <t:value-of select="$header"/>
        </div>
        <ol>
          <t:for-each select="$items">
            <t:sort select="&sort-key;"/>
            <t:variable name="same-name-items"
                select="$items[(self::doc:*/@name | @doc:name) =
                               (current ()/self::doc:*/@name |
                                    current ()/@doc:name)]"/>
            <t:if test="generate-id ($same-name-items) =
                        generate-id (self::node ())">
              <t:call-template name="index-item">
                <t:with-param name="items" select="$same-name-items"/>
              </t:call-template>
            </t:if>
          </t:for-each>
        </ol>
      </li>
    </t:if>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'gnu-fdl-summary']" mode="h2">
    <t:for-each select="/descendant::doc:insert[@content = 'gnu-fdl']
                        /ancestor::doc:appendix">
      <t:variable name="id" select="@xml:id"/>
      
      <div class="non-para" lang="en" xml:lang="en"
      >Copyright &#x00A9; <t:value-of select="/child::doc:document/@published-year"
      /><!-- YOUR NAME -->.</div>
      
      <p lang="en" xml:lang="en">Permission is granted to copy, distribute
      and/or modify this document
      under the terms of the GNU Free Documentation License, Version 1.1
      or any later version published by the <a
      href="http://www.fsf.org/">Free Software Foundation</a>;
      with no <a href="#{$id}-gnu-fdl-invariant-sections">Invariant Sections</a>,
      with no <a href="#{$id}-gnu-fdl-front-cover-texts">Front$B!>(BCover Texts</a>,
      and with no <a href="#{$id}-gnu-fdl-back-cover-texts">Back$B!>(BCover Texts</a>.
      A copy of the license is included in the section entitled 
      <cite class="section">
        <a href="#{$id}-gnu-fdl">GNU Free Documentation License</a>
      </cite>.</p>
    </t:for-each>
  </t:template>
  
  <t:template match="child::doc:insert[@content = 'gnu-fdl']" mode="h2">
    <t:variable name="id" select="ancestor::doc:appendix/@xml:id"/>
    <div class="section document-gnu-fdl" id="{$id}-gnu-fdl"
        lang="en" xml:lang="en">
        <h3>GNU Free Documentation License</h3>
        
        <div class="non-para">Version 1.1, March 2000</div>
        
        <p>
          <span class="line">Copyright &#x00A9; 2000  Free Software Foundation,
          Inc.</span>
          <span class="line">51 Franklin St, Fifth Floor, Boston, MA  02110-1301  
          USA</span>
          <span class="line">Everyone is permitted to copy and distribute verbatim 
          copies of this license document, but changing it is not allowed.</span>
        </p>
      
      <div class="section">
      <h4>0. PREAMBLE</h4>
      
      <p>The purpose of this License is to make a manual, textbook, or other written document <q>free</q> in the sense of freedom: to assure everyone the effective freedom to copy and redistribute it, with or without modifying it, either commercially or noncommercially. Secondarily, this License preserves for the author and publisher a way to get credit for their work, while not being considered responsible for modifications made by others.</p>
      
      <p>This License is a kind of <q>copyleft</q>, which means that derivative works of the document must themselves be free in the same sense. It complements the GNU General Public License, which is a copyleft license designed for free software.</p>
      
      <p>We have designed this License in order to use it for manuals for free software, because free software needs free documentation: a free program should come with manuals providing the same freedoms that the software does. But this License is not limited to software manuals; it can be used for any textual work, regardless of subject matter or whether it is published as a printed book. We recommend this License principally for works whose purpose is instruction or reference.</p>
      </div>
      
      <div class="section">
      <h4>1. APPLICABILITY AND DEFINITIONS</h4>

      <p>This License applies to any manual or other work that contains a notice placed by the copyright holder saying it can be distributed under the terms of this License. The <q>Document</q>, below, refers to any such manual or work. Any member of the public is a licensee, and is addressed as <q>you</q>.</p>
      
      <p>A <q>Modified Version</q> of the Document means any work containing the Document or a portion of it, either copied verbatim, or with modifications and/or translated into another language.</p>
      
      <p>A <q>Secondary Section</q> is a named appendix or a front-matter section of the Document that deals exclusively with the relationship of the publishers or authors of the Document to the Document's overall subject (or to related matters) and contains nothing that could fall directly within that overall subject. (For example, if the Document is in part a textbook of mathematics, a Secondary Section may not explain any mathematics.) The relationship could be a matter of historical connection with the subject or with related matters, or of legal, commercial, philosophical, ethical or political position regarding them.</p>
      
      <p id="{$id}-gnu-fdl-invariant-sections">The <q>Invariant Sections</q> are certain Secondary Sections whose titles are designated, as being those of Invariant Sections, in the notice that says that the Document is released under this License.</p>
      
      <p>The <q>Cover Texts</q> are certain short passages of text that are listed, as <i id="{$id}-gnu-fdl-front-cover-texts">Front$B!>(BCover Texts</i> or <i id="{$id}-gnu-fdl-back-cover-texts">Back$B!>(BCover Texts</i>, in the notice that says that the Document is released under this License.</p>
      
      <p>A <q>Transparent</q> copy of the Document means a machine-readable copy, represented in a format whose specification is available to the general public, whose contents can be viewed and edited directly and straightforwardly with generic text editors or (for images composed of pixels) generic paint programs or (for drawings) some widely available drawing editor, and that is suitable for input to text formatters or for automatic translation to a variety of formats suitable for input to text formatters. A copy made in an otherwise Transparent file format whose markup has been designed to thwart or discourage subsequent modification by readers is not Transparent. A copy that is not <q>Transparent</q> is called <q>Opaque</q>.</p>
      
      <p>Examples of suitable formats for Transparent copies include plain ASCII without markup, Texinfo input format, <span
      class="latex">L<span class="latex-a">a</span>T<span
      class="latex-e">e</span>X</span> input format, SGML or XML using a publicly available DTD, and standard-conforming simple HTML designed for human modification. Opaque formats include PostScript, PDF, proprietary formats that can be read and edited only by proprietary word processors, SGML or XML for which the DTD and/or processing tools are not generally available, and the machine$B!>(Bgenerated HTML produced by some word processors for output purposes only.</p>
      
      <p>The <q>Title Page</q> means, for a printed book, the title page itself, plus such following pages as are needed to hold, legibly, the material this License requires to appear in the title page. For works in formats which do not have any title page as such, <q>Title Page</q> means the text near the most prominent appearance of the work's title, preceding the beginning of the body of the text.</p>
      </div>
      
      <div class="section">
      <h4>2. VERBATIM COPYING</h4>
      
      <p>You may copy and distribute the Document in any medium, either commercially or noncommercially, provided that this License, the copyright notices, and the license notice saying this License applies to the Document are reproduced in all copies, and that you add no other conditions whatsoever to those of this License. You may not use technical measures to obstruct or control the reading or further copying of the copies you make or distribute. However, you may accept compensation in exchange for copies. If you distribute a large enough number of copies you must also follow the conditions in section 3.</p>
      
      <p>You may also lend copies, under the same conditions stated above, and you may publicly display copies.</p>
      </div>
      
      <div class="section">
      <h4>3. COPYING IN QUANTITY</h4>
      
      <p>If you publish printed copies of the Document numbering more than 100, and the Document's license notice requires Cover Texts, you must enclose the copies in covers that carry, clearly and legibly, all these Cover Texts: Front$B!>(BCover Texts on the front cover, and Back$B!>(BCover Texts on the back cover. Both covers must also clearly and legibly identify you as the publisher of these copies. The front cover must present the full title with all words of the title equally prominent and visible. You may add other material on the covers in addition. Copying with changes limited to the covers, as long as they preserve the title of the Document and satisfy these conditions, can be treated as verbatim copying in other respects.</p>
      
      <p>If the required texts for either cover are too voluminous to fit legibly, you should put the first ones listed (as many as fit reasonably) on the actual cover, and continue the rest onto adjacent pages.</p>
      
      <p>If you publish or distribute Opaque copies of the Document numbering more than 100, you must either include a machine$B!>(Breadable Transparent copy along with each Opaque copy, or state in or with each Opaque copy a publicly$B!>(Baccessible computer$B!>(Bnetwork location containing a complete Transparent copy of the Document, free of added material, which the general network-using public has access to download anonymously at no charge using public-standard network protocols. If you use the latter option, you must take reasonably prudent steps, when you begin distribution of Opaque copies in quantity, to ensure that this Transparent copy will remain thus accessible at the stated location until at least one year after the last time you distribute an Opaque copy (directly or through your agents or retailers) of that edition to the public.</p>
      
      <p>It is requested, but not required, that you contact the authors of the Document well before redistributing any large number of copies, to give them a chance to provide you with an updated version of the Document.</p>
      </div>
      
      <div class="section">
      <h4>4. MODIFICATIONS</h4>
      
      <p>You may copy and distribute a Modified Version of the Document under the conditions of sections 2 and 3 above, provided that you release the Modified Version under precisely this License, with the Modified Version filling the role of the Document, thus licensing distribution and modification of the Modified Version to whoever possesses a copy of it. In addition, you must do these things in the Modified Version:</p>
      
      <ul class="has-marker">
      <li><span class="marker">A.</span> Use in the Title Page (and on the covers, if any) a title distinct from that of the Document, and from those of previous versions (which should, if there were any, be listed in the History section of the Document). You may use the same title as a previous version if the original publisher of that version gives permission.</li>
      <li><span class="marker">B.</span> List on the Title Page, as authors, one or more persons or entities responsible for authorship of the modifications in the Modified Version, together with at least five of the principal authors of the Document (all of its principal authors, if it has less than five).</li>
      <li><span class="marker">C.</span> State on the Title page the name of the publisher of the Modified Version, as the publisher.</li>
      <li><span class="marker">D.</span> Preserve all the copyright notices of the Document.</li>
      <li><span class="marker">E.</span> Add an appropriate copyright notice for your modifications adjacent to the other copyright notices.</li>
      <li><span class="marker">F.</span> Include, immediately after the copyright notices, a license notice giving the public permission to use the Modified Version under the terms of this License, in the form shown in the Addendum below.</li>
      <li><span class="marker">G.</span> Preserve in that license notice the full lists of Invariant Sections and required Cover Texts given in the Document's license notice.</li>
      <li><span class="marker">H.</span> Include an unaltered copy of this License.</li>
      <li><span class="marker">I.</span> Preserve the section entitled <q>History</q>, and its title, and add to it an item stating at least the title, year, new authors, and publisher of the Modified Version as given on the Title Page. If there is no section entitled <q>History</q> in the Document, create one stating the title, year, authors, and publisher of the Document as given on its Title Page, then add an item describing the Modified Version as stated in the previous sentence.</li>
      <li><span class="marker">J.</span> Preserve the network location, if any, given in the Document for public access to a Transparent copy of the Document, and likewise the network locations given in the Document for previous versions it was based on. These may be placed in the <q>History</q> section. You may omit a network location for a work that was published at least four years before the Document itself, or if the original publisher of the version it refers to gives permission.</li>
      <li><span class="marker">K.</span> In any section entitled <q>Acknowledgements</q> or <q>Dedications</q>, preserve the section's title, and preserve in the section all the substance and tone of each of the contributor acknowledgements and/or dedications given therein.</li>
      <li><span class="marker">L.</span> Preserve all the Invariant Sections of the Document, unaltered in their text and in their titles. Section numbers or the equivalent are not considered part of the section titles.</li>
      <li><span class="marker">M.</span> Delete any section entitled <q>Endorsements</q>. Such a section may not be included in the Modified Version.</li>
      <li><span class="marker">N.</span> Do not retitle any existing section as <q>Endorsements</q> or to conflict in title with any Invariant Section.</li>
    </ul>
    
    <p>If the Modified Version includes new front$B!>(Bmatter sections or appendices that qualify as Secondary Sections and contain no material copied from the Document, you may at your option designate some or all of these sections as invariant. To do this, add their titles to the list of Invariant Sections in the Modified Version's license notice. These titles must be distinct from any other section titles.</p>
    
    <p>You may add a section entitled <q>Endorsements</q>, provided it contains nothing but endorsements of your Modified Version by various parties$B!=!=(Bfor example, statements of peer review or that the text has been approved by an organization as the authoritative definition of a standard.</p>
    
    <p>You may add a passage of up to five words as a Front$B!>(BCover Text, and a passage of up to 25 words as a Back$B!>(BCover Text, to the end of the list of Cover Texts in the Modified Version. Only one passage of Front$B!>(BCover Text and one of Back$B!>(BCover Text may be added by (or through arrangements made by) any one entity. If the Document already includes a cover text for the same cover, previously added by you or by arrangement made by the same entity you are acting on behalf of, you may not add another; but you may replace the old one, on explicit permission from the previous publisher that added the old one.</p>
    
    <p>The author(s) and publisher(s) of the Document do not by this License give permission to use their names for publicity for or to assert or imply endorsement of any Modified Version.</p>
    </div>
    
    <div class="section">
    <h4>5. COMBINING DOCUMENTS</h4>
    
    <p>You may combine the Document with other documents released under this License, under the terms defined in section 4 above for modified versions, provided that you include in the combination all of the Invariant Sections of all of the original documents, unmodified, and list them all as Invariant Sections of your combined work in its license notice.</p>
    
    <p>The combined work need only contain one copy of this License, and multiple identical Invariant Sections may be replaced with a single copy. If there are multiple Invariant Sections with the same name but different contents, make the title of each such section unique by adding at the end of it, in parentheses, the name of the original author or publisher of that section if known, or else a unique number. Make the same adjustment to the section titles in the list of Invariant Sections in the license notice of the combined work.</p>
    
    <p>In the combination, you must combine any sections entitled <q>History</q> in the various original documents, forming one section entitled <q>History</q>; likewise combine any sections entitled <q>Acknowledgements</q>, and any sections entitled <q>Dedications</q>. You must delete all sections entitled <q>Endorsements.</q></p>
    </div>
    
    <div class="section">
    <h4>6. COLLECTIONS OF DOCUMENTS</h4>
    
    <p>You may make a collection consisting of the Document and other documents released under this License, and replace the individual copies of this License in the various documents with a single copy that is included in the collection, provided that you follow the rules of this License for verbatim copying of each of the documents in all other respects.</p>
    
    <p>You may extract a single document from such a collection, and distribute it individually under this License, provided you insert a copy of this License into the extracted document, and follow this License in all other respects regarding verbatim copying of that document.</p>
    </div>
    
    <div class="section">
    <h4>7. AGGREGATION WITH INDEPENDENT WORKS</h4>
    
    <p>A compilation of the Document or its derivatives with other separate and independent documents or works, in or on a volume of a storage or distribution medium, does not as a whole count as a Modified Version of the Document, provided no compilation copyright is claimed for the compilation. Such a compilation is called an <q>aggregate</q>, and this License does not apply to the other self$B!>(Bcontained works thus compiled with the Document, on account of their being thus compiled, if they are not themselves derivative works of the Document.</p>
    
    <p>If the Cover Text requirement of section 3 is applicable to these copies of the Document, then if the Document is less than one quarter of the entire aggregate, the Document's Cover Texts may be placed on covers that surround only the Document within the aggregate. Otherwise they must appear on covers around the whole aggregate.</p>
    </div>
    
    <div class="section">
    <h4>8. TRANSLATION</h4>
    
    <p>Translation is considered a kind of modification, so you may distribute translations of the Document under the terms of section 4. Replacing Invariant Sections with translations requires special permission from their copyright holders, but you may include translations of some or all Invariant Sections in addition to the original versions of these Invariant Sections. You may include a translation of this License provided that you also include the original English version of this License. In case of a disagreement between the translation and the original English version of this License, the original English version will prevail.</p>
    </div>
    
    <div class="section">
    <h4>9. TERMINATION</h4>
    
    <p>You may not copy, modify, sublicense, or distribute the Document except as expressly provided for under this License. Any other attempt to copy, modify, sublicense or distribute the Document is void, and will automatically terminate your rights under this License. However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.</p>
    </div>
    
    <div class="section">
    <h4>10. FUTURE REVISIONS OF THIS LICENSE</h4>
    
    <p>The Free Software Foundation may publish new, revised versions of the GNU Free Documentation License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns. See <code class="uri"><a href="http://www.gnu.org/copyleft/">http://www.gnu.org/copyleft/</a></code>.</p>
    
    <p>Each version of the License is given a distinguishing version number. If the Document specifies that a particular numbered version of this License <q>or any later version</q> applies to it, you have the option of following the terms and conditions either of that specified version or of any later version that has been published (not as a draft) by the Free Software Foundation. If the Document does not specify a version number of this License, you may choose any version ever published (not as a draft) by the Free Software Foundation.</p>
    </div>
    </div>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="title">
    <title>
      <t:apply-templates select="self::*" mode="lang"/>
      <t:value-of select="self::*"/>
    </title>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="attr">
    <t:value-of select="self::*"/>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="h1">
    <h1>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </h1>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="h2">
    <h2>
      <t:apply-templates select="@*"/>
      <t:number count="html5:section" format="1. "/>
      <t:apply-templates/>
    </h2>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="h3">
    <h3>
      <t:apply-templates select="@*"/>
      <t:number level="multiple" count="html5:section" format="1. "/>
      <t:apply-templates/>
    </h3>
  </t:template>
  
  <t:template match="child::xhtml2:h" mode="toc">
    <span>
      <t:apply-templates select="@*"/>
      <t:number level="multiple" count="html5:section" format="1. "/>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="doc:appendix/child::xhtml2:h" mode="h2">
    <h2>
      <t:apply-templates select="@*"/>
      <t:number count="doc:appendix" format="A. "/>
      <t:apply-templates/>
    </h2>
  </t:template>
  
  <t:template match="doc:appendix//xhtml2:h" mode="h3">
    <h3>
      <t:apply-templates select="@*"/>
      <t:number count="doc:appendix" format="A."/>
      <t:number count="html5:section" format="1. "/>
      <t:apply-templates/>
    </h3>
  </t:template>
  
  <t:template match="doc:appendix//xhtml2:h" mode="toc">
    <span>
      <t:apply-templates select="@*"/>
      <t:number count="doc:appendix" format="A."/>
      <t:choose>
      <t:when test="parent::html5:section">
        <t:number count="html5:section" format="1. "/>
      </t:when>
      <t:otherwise><t:value-of select="' '"/></t:otherwise>
      </t:choose>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="doc:presection/child::xhtml2:h" mode="h2">
    <h2>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </h2>
  </t:template>
  
  <t:template match="doc:presection//xhtml2:h" mode="toc">
    <span>
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="doc:quote//xhtml2:h">
    <h4>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </h4>
  </t:template>
  
  <t:template match="child::html5:section" mode="h1">
    <div class="section">
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="h2"/>
    </div>
  </t:template>
  
  <t:template match="child::doc:presection" mode="h1">
    <div class="section">
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="h2"/>
    </div>
  </t:template>
  
  <t:template match="child::html5:section" mode="h2">
    <div class="section">
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="h3"/>
    </div>
  </t:template>
  
  <t:template match="child::html5:section |
                     child::doc:appendix |
                     child::doc:presection" mode="toc">
    <li class="doc-{local-name ()}">
      <t:apply-templates select="self::node ()" mode="toc-value"/>
    </li>
  </t:template>
  
  <t:template match="child::html5:section |
                     child::doc:appendix |
                     child::doc:presection" mode="toc-detail">
    <li class="doc-{local-name ()}">
      <t:apply-templates select="self::node ()" mode="toc-value"/>
      <t:if test="child::html5:section">
        <ol>
          <t:apply-templates select="child::html5:section" mode="toc-detail"/>
        </ol>
      </t:if>
    </li>
  </t:template>
  
  <t:template match="child::html5:section |
                     child::doc:appendix |
                     child::doc:presection" mode="toc-value">
    <span class="doc-{local-name ()}-value">
      <a href="#{@xml:id}">
        <t:apply-templates select="child::xhtml2:h" mode="toc"/>
      </a>
      <t:if test="child::doc:keyword">
        <t:value-of select="' ('"/>
        <t:apply-templates select="child::doc:keyword[position () = 1]"
                           mode="toc"/>
        <t:if test="child::doc:keyword[position () = 2]">
          <t:for-each select="child::doc:keyword[position () != 1]">
            <t:value-of select="', '"/>
            <t:apply-templates select="self::node ()" mode="toc"/>
          </t:for-each>
        </t:if>
        <t:value-of select="')'"/>
      </t:if>
    </span>
  </t:template>
  
  <t:template match="child::html5:section" mode="answer-h3">
    <t:if test="child::doc:exercise">
      <div class="section">
        <t:apply-templates select="child::xhtml2:h" mode="h3"/>
        <t:apply-templates select="child::doc:exercise" mode="answer-h4"/>
      </div>
    </t:if>
  </t:template>
  
  <t:template match="child::doc:appendix" mode="h1">
    <div class="section appendix">
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="h2"/>
    </div>
  </t:template>
  
  <t:template match="child::doc:appendix" mode="h2">
    <div class="section appendix">
      <t:apply-templates select="@*"/>
      <t:apply-templates mode="h3"/>
    </div>
  </t:template>
  
  <t:template match="child::doc:b">
    <div class="doc-section-body">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:keyword" mode="h2"/>
  
  <t:template match="child::doc:keyword" mode="toc">
    <span class="doc-keyword">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </span>
  </t:template>
  
  <t:template match="child::doc:preamble">
    <div class="doc-preamble">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:preamble" mode="h1">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:preamble" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:preamble" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:postamble" mode="h2">
    <div class="doc-postamble">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:exercise" mode="h2">
    <div class="section doc-exercise"
        id="{ancestor::html5:section/@xml:id}-exercise">
      <t:apply-templates select="@*"/>
      <h2 lang="ja" xml:lang="ja">$B1i=,LdBj(B</h2>
      <t:apply-templates mode="h3"/>
    </div>
  </t:template>
  
  <t:template match="child::doc:exercise" mode="answer-h4">
    <t:apply-templates select="child::doc:problem" mode="answer-h4"/>
  </t:template>
  
  <t:template match="child::doc:problem" mode="h3">
    <div class="section doc-problem">
      <t:apply-templates select="@*"/>
      <h3 lang="ja" xml:lang="ja">$BLd(B<t:number count="doc:problem" format="1"/></h3>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:problem" mode="answer-h4">
    <div class="section doc-problem" id="answers-{@xml:id}">
      <h4 lang="ja" xml:lang="ja">
        <a href="#{@xml:id}">$BLd(B<t:number count="doc:problem" format="1"/></a>
      </h4>
      <t:choose>
      <t:when test="child::doc:answer">
        <t:apply-templates select="child::doc:answer" mode="answer"/>
      </t:when>
      <t:otherwise>
        <p>$B>JN,!#(B</p>
      </t:otherwise>
      </t:choose>
    </div>
  </t:template>
  
  <t:template match="child::doc:answer"/>
  
  <t:template match="child::doc:answer" mode="answer">
    <t:apply-templates/>
  </t:template>
  
  <t:template match="child::doc:note">
    <div class="info memo">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:note" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:note" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:ed">
    <div class="info memo ed" id="ed-{generate-id (self::node ())}">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:ed" mode="h1">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:ed" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:ed" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:todo">
    <div class="info memo ed todo" id="ed-{generate-id (self::node ())}">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:todo" mode="h1">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:todo" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:todo" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="doc:ed | doc:todo" mode="todo-list">
    <li>
      <t:if test="ancestor::html5:section | ancestor::doc:appendix">
        <t:value-of select="'$B!x(B'"/>
        <t:if test="ancestor::doc:appendix">
          <t:number count="doc:appendix" format="A"/>
          <t:if test="ancestor::html5:section">
            <t:value-of select="'.'"/>
          </t:if>
        </t:if>
        <t:number count="html5:section" level="multiple" format="1.1"/>
        <t:value-of select="' '"/>
      </t:if>
      <a href="#ed-{generate-id (self::node ())}">
        <t:value-of select="substring (normalize-space (self::node ()), 0, 30)"/>
        <t:if test="string-length (normalize-space (self::node ())) > 30">
          <span class="snip">...</span>
        </t:if>
      </a>
    </li>
  </t:template>
  
  <t:template match="child::doc:keyPoints">
    <div class="doc-key-points" id="{parent::node ()/@xml:id}-keypoints">
      <t:apply-templates select="@*"/>
      <div class="caption">$B!x(B<t:number count="html5:section" level="multiple"
      format="1.1"/> $B$N$^$H$a(B</div>
      <ul>
        <t:apply-templates/>
      </ul>
    </div>
  </t:template>
  <t:template match="child::doc:keyPoints" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:keyPoints" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:keyPoints" mode="toc-keypoints">
    <li>
      <a href="#{parent::node ()/@xml:id}-keypoints"
      >$B!x(B<t:number count="html5:section" level="multiple"
      format="1.1"/> <cite class="section">
        <t:apply-templates select="parent::node ()/child::xhtml2:h/@*" mode="text"/>
        <t:apply-templates select="parent::node ()/child::xhtml2:h/child::node ()"
            mode="text"/>
      </cite>$B$N$^$H$a(B</a>
    </li>
  </t:template>
  
  <t:template match="child::html5:ul">
    <ul>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </ul>
  </t:template>
  <t:template match="child::html5:ol">
    <ol>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </ol>
  </t:template>
  <t:template match="child::html5:dl">
    <dl>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </dl>
  </t:template>
  <t:template match="child::html5:ul |
                     child::html5:ol |
                     child::html5:dl" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::html5:ul |
                     child::html5:ol |
                     child::html5:dl" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::html5:li">
    <li>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </li>
  </t:template>
  
  <t:template match="child::xhtml2:di">
    <!--<di>
      <t:apply-templates select="@*"/>-->
      <t:apply-templates/>
    <!--</di>-->
  </t:template>
  
  <t:template match="child::html5:dt">
    <dt>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </dt>
  </t:template>
  
  <t:template match="child::html5:dd">
    <dd>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </dd>
  </t:template>
  
  <t:template match="child::doc:quote">
    <div class="fig quote">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:quote" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:quote" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:quote" mode="toc-figures">
    <li>
      <a href="#{@xml:id}">
        <t:apply-templates select="child::doc:quoteCredit" mode="toc"/>
      </a>
    </li>
  </t:template>
  
  <t:template match="doc:miniColumn" mode="figure-number"/>
  
  <t:template match="child::doc:quoteBody">
    <blockquote><!-- @@ html5 semantics ?? -->
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </blockquote>
  </t:template>
  
  <t:template match="child::doc:quoteCredit">
    <div class="credit">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:quoteCredit" mode="toc">
    <span lang="ja" xml:lang="ja">$B0zMQ(B</span>
    <t:value-of select="' '"/>
    <span>
      <t:apply-templates select="@*" mode="text"/>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="child::doc:quoteCredit" mode="text">
    <span lang="ja" xml:lang="ja">$B0zMQ(B</span>
    <t:value-of select="' '"/>
    <span>
      <t:apply-templates select="@*" mode="text"/>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="child::doc:quoteCredit" mode="attr">
    <t:value-of select="'$B0zMQ(B '"/>
    <t:apply-templates mode="attr"/>
  </t:template>
  
  <t:template match="child::doc:fig">
    <div class="fig">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:fig" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:fig" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:fig" mode="clone">
    <div class="fig doc-clone">
      <t:apply-templates select="@*[not (namespace-uri () = 'http://www.w3.org/XML/1998/namespace' and local-name () = 'id')]"/>
      <t:apply-templates mode="clone"/>
    </div>
  </t:template>
  
  <t:template match="child::doc:fig |
                     child::doc:example" mode="toc-figures">
    <li>
      <a href="#{@xml:id}">
        <t:apply-templates select="child::doc:caption" mode="toc"/>
      </a>
    </li>
  </t:template>
  
  <t:template match="doc:fig |
                     doc:example" mode="figure-number">
    <span lang="ja" xml:lang="ja">$B?^(B</span>
    <t:value-of select="' '"/>
    <t:if test="ancestor::doc:appendix">
      <t:number count="doc:appendix" format="A."/>
    </t:if>
    <t:number level="multiple" count="html5:section" format="1.1"/>
    <t:number count="doc:fig | doc:example" format=".1"/>
  </t:template>
  
  <t:template match="doc:fig |
                     doc:example" mode="figure-number-attr">
    <t:value-of select="'$B?^(B '"/>
    <t:if test="ancestor::doc:appendix">
      <t:number count="doc:appendix" format="A."/>
    </t:if>
    <t:number level="multiple" count="html5:section" format="1.1"/>
    <t:number count="doc:fig | doc:example" format=".1"/>
  </t:template>
  
  <t:template match="child::doc:figBody">
    <div class="fig-body">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:figBody" mode="clone">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:figClone">
    <t:apply-templates mode="clone"
        select="/descendant::doc:fig[string (@xml:id) = string (current ()/@of)]"/>
  </t:template>
  <t:template match="child::doc:figClone" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:figClone" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:example">
    <div class="example">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  <t:template match="child::doc:example" mode="h2">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  <t:template match="child::doc:example" mode="h3">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:miniColumn" mode="h3">
    <div class="doc-mini-column">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:miniColumn" mode="toc-columns">
    <li>
      <a href="#{@xml:id}">
        <t:apply-templates select="child::doc:caption" mode="toc"/>
      </a>
    </li>
  </t:template>
  
  <t:template match="doc:miniColumn" mode="figure-number"/>
  
  <t:template match="child::doc:caption">
    <div class="caption">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="doc:fig/doc:caption |
                     doc:example/doc:caption">
    <div class="caption">
      <t:apply-templates select="@*"/>
      <t:apply-templates select="parent::node ()" mode="figure-number"/>
      <t:value-of select="' '"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::doc:caption" mode="clone">
    <div class="caption">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
      <t:value-of select="' '"/>
      <span lang="ja" xml:lang="ja">($B:F7G(B)</span>
    </div>
  </t:template>
  
  <t:template match="doc:fig/doc:caption |
                     doc:example/doc:caption" mode="clone">
    <div class="caption">
      <t:apply-templates select="@*"/>
      <t:apply-templates select="parent::node ()" mode="figure-number"/>
      <t:value-of select="' '"/>
      <t:apply-templates/>
      <t:value-of select="' '"/>
      <span lang="ja" xml:lang="ja">($B:F7G(B)</span>
    </div>
  </t:template>
  
  <t:template match="child::doc:caption" mode="toc">
    <t:apply-templates select="parent::node ()" mode="figure-number"/>
    <t:value-of select="' '"/>
    <span>
      <t:apply-templates select="@*" mode="text"/>
      <t:apply-templates mode="text"/>
    </span>
  </t:template>
  
  <t:template match="child::doc:caption" mode="text">
    <t:apply-templates select="parent::node ()" mode="figure-number"/>
    <t:value-of select="' '"/>
    <cite class="section">
      <t:apply-templates select="@*" mode="text"/>
      <t:apply-templates mode="text"/>
    </cite>
  </t:template>
  
  <t:template match="child::doc:caption" mode="attr">
    <t:apply-templates select="parent::node ()" mode="figure-number-attr"/>
    <t:value-of select="' $B!V(B'"/>
    <t:apply-templates mode="attr"/>
    <t:value-of select="'$B!W(B'"/>
  </t:template>
  
  <t:template match="child::html5:p">
    <p>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </p>
  </t:template>
  <t:template match="child::html5:p" mode="h2">
    <p>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </p>
  </t:template>
  <t:template match="child::html5:p" mode="h3">
    <p>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </p>
  </t:template>
  
  <t:template match="html5:p//doc:syntax">
    <span class="block tutorial-syntax">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </span>
  </t:template>
  
  <t:template match="child::doc:snip">
    <div class="non-para snip">
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <t:attribute name="lang">ja</t:attribute>
        <t:attribute name="xml:lang">ja</t:attribute>
        <t:value-of select="'($BCfN,(B)'"/>
      </t:otherwise>
      </t:choose>
    </div>
  </t:template>
  
  <t:template match="html5:p//doc:snip | html5:li//doc:snip |
                     tree:content//doc:snip | tree:leaf//doc:snip |
                     doc:htmlCode//doc:snip">
    <span class="snip">
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <t:attribute name="lang">ja</t:attribute>
        <t:attribute name="xml:lang">ja</t:attribute>
        <t:value-of select="'$B!&!&!&!&!&!&(B'"/>
      </t:otherwise>
      </t:choose>
    </span>
  </t:template>
  
  <t:template match="child::tree:root">
    <ol class="xoxo">
      <t:apply-templates select="@*"/>
      <li class="tree-root doc-node-type-{@doc:nodeType}">
        <t:apply-templates select="child::tree:content"/>
        <t:if test="child::tree:leaf | child::tree:node">
          <ol>
            <t:apply-templates select="child::tree:leaf | child::tree:node"/>
          </ol>
        </t:if>
      </li>
    </ol>
  </t:template>
  
  <t:template match="child::tree:node">
    <li class="tree-node doc-node-type-{@doc:nodeType}">
      <t:apply-templates select="@*"/>
      <t:apply-templates select="child::tree:content"/>
      <t:if test="child::tree:leaf | child::tree:node">
        <ol>
          <t:apply-templates select="child::tree:leaf | child::tree:node"/>
        </ol>
      </t:if>
    </li>
  </t:template>
  
  <t:template match="child::tree:content">
    <div class="non-para tree-content">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </div>
  </t:template>
  
  <t:template match="child::tree:leaf">
    <li class="tree-leaf doc-node-type-{@doc:nodeType}">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </li>
  </t:template>
  
  <t:template match="@doc:nodeType"/>
  
  <t:template match="child::doc:he">
    <code class="HTMLe">
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="not (parent::html5:dfn) and
                    not (parent::doc:iw) and
                    not (parent::doc:markRef) and
                    /descendant::html5:dfn
                     [string (@doc:name) = concat ('html-', string (current ()))]">
        <a href="#dt-html-{string (self::*)}">
          <t:apply-templates/>
        </a>
      </t:when>
      <t:otherwise>
        <t:apply-templates/>
      </t:otherwise>
      </t:choose>
    </code>
  </t:template>
  
  <t:template match="child::doc:he" mode="text">
    <code class="HTMLe">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
    <t:value-of select="' '"/>
    <span lang="ja" xml:lang="ja">(<abbr
    lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr>
    $BMWAG7?(B)</span>
  </t:template>
  
  <t:template match="child::doc:ha">
    <code class="HTMLa">
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="@name and
                    not (parent::html5:dfn) and
                    not (parent::doc:iw) and
                    not (parent::doc:markRef) and
                    /descendant::html5:dfn
                     [string (@doc:name) = string (current ()/@name)]">
        <a href="#dt-{string (@name)}">
          <t:apply-templates/>
        </a>
      </t:when>
      <t:when test="not (parent::html5:dfn) and
                    not (parent::doc:iw) and
                    not (parent::doc:markRef) and
                    /descendant::html5:dfn
                     [string (@doc:name) =
                      concat ('html-attr-', string (current ()))]">
        <a href="#dt-html-attr-{string (self::*)}">
          <t:apply-templates/>
        </a>
      </t:when>
      <t:otherwise>
        <t:apply-templates/>
      </t:otherwise>
      </t:choose>
    </code>
  </t:template>
  <t:template match="doc:ha/@name"/>
  
  <t:template match="child::doc:ha" mode="text">
    <code class="HTMLa">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
    <t:value-of select="' '"/>
    <span lang="ja" xml:lang="ja">(<abbr
    lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr>
    $BB0@-(B)</span>
  </t:template>
  <t:template match="doc:ha/@name" mode="text"/>
  
  
  <t:template match="child::doc:cp">
    <code class="CSS">
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="not (parent::html5:dfn) and
                    not (parent::doc:iw) and
                    not (parent::doc:markRef) and
                    /descendant::html5:dfn
                     [string (@doc:name) =
                      concat ('css-prop-', string (current ()))]">
        <a href="#dt-css-prop-{string (self::*)}">
          <t:apply-templates/>
        </a>
      </t:when>
      <t:otherwise>
        <t:apply-templates/>
      </t:otherwise>
      </t:choose>
    </code>
  </t:template>
  
  <t:template match="child::doc:cp" mode="text">
    <code class="CSS">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
    <t:value-of select="' '"/>
    <span lang="ja" xml:lang="ja">(<abbr
    lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr>
    $BFC@-(B)</span>
  </t:template>
  
  <t:template match="child::html5:dfn">
    <dfn>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </dfn>
  </t:template>
  
  <t:template name="index-item">
    <t:param name="items"/>
    <t:variable name="rep-item"
        select="$items[self::html5:dfn and position () = 1]"/>
    <t:variable name="other-item"
        select="$items[not (self::html5:dfn and position () = 1)]"/>
    <li>
      <span class="doc-index-item">
        <t:choose>
        <t:when test="$rep-item">
          <t:for-each select="$rep-item">
            <a href="#dt-{self::doc:*/@name | @doc:name}">
              <t:apply-templates select="@xml:lang"/>
              <t:apply-templates mode="text"/>
            </a>
          </t:for-each>
        </t:when>
        <t:otherwise>
          <a>
            <t:apply-templates select="@xml:lang"/>
            <t:apply-templates select="$items[position () = 1]/child::node ()"
                mode="text"/>
          </a>
        </t:otherwise>
        </t:choose>
        <t:if test="self::doc:*/@en | @doc:en">
          <t:value-of select="' ('"/>
          <span lang="en" xml:lang="en">
            <t:value-of select="self::doc:*/@en | @doc:en"/>
          </span>
          <t:value-of select="')'"/>
        </t:if>
      </span>
      
      <span class="doc-index-page">
        <t:value-of select="' '"/>
        <t:apply-templates select="$items[position () = 1]" mode="index-page"/>
        <t:for-each select="$items[position () != 1]">
          <t:value-of select="', '"/>
          <t:apply-templates select="self::node ()" mode="index-page"/>
        </t:for-each>
      </span>
    </li>
  </t:template>
  
  <t:template match="html5:dfn" mode="index-page">
    <em><a href="#dt-{@doc:name}">
      <t:apply-templates select="self::node ()" mode="index-page-section"/>
    </a></em>
  </t:template>
  
  <t:template match="doc:iw" mode="index-page">
    <a href="#index-{generate-id (self::node ())}">
      <t:apply-templates select="self::node ()" mode="index-page-section"/>
    </a>
  </t:template>
  
  <t:template match="html5:dfn | doc:iw" mode="index-page-section">
    <t:attribute name="title">
      <t:choose>
      <t:when test="ancestor::html5:section">
        <t:apply-templates select="ancestor::html5:section[position () = 1]/
                                   child::xhtml2:h" mode="attr"/>
      </t:when>
      <t:when test="ancestor::doc:presection">
        <t:apply-templates select="ancestor::doc:presection[position () = 1]/
                                   child::xhtml2:h" mode="attr"/>
      </t:when>
      <t:otherwise>
        <t:apply-templates select="ancestor::doc:appendix[position () = 1]/
                                   child::xhtml2:h" mode="attr"/>
      </t:otherwise>
      </t:choose>
    </t:attribute>
      <t:value-of select="'$B!x(B'"/>
      <t:if test="ancestor::doc:appendix">
        <t:number count="doc:appendix" format="A."/>
      </t:if>
      <t:number count="html5:section" level="multiple" format="1.1"/>
      <t:if test="ancestor::doc:note">
        <t:value-of select="' '"/>
        <span lang="ja" xml:lang="ja">$B%a%b(B</span>
      </t:if>
  </t:template>
  
  <t:template match="html5:dfn/@doc:name" mode="text"/>
  <t:template match="html5:dfn/@doc:key" mode="text"/>
  <t:template match="html5:dfn/@doc:en" mode="text"/>
  <t:template match="doc:iw/@name" mode="text"/>
  <t:template match="doc:iw/@key" mode="text"/>
  <t:template match="doc:iw/@en" mode="text"/>
  
  <t:template match="html5:dfn/@doc:name">
    <t:attribute name="id">dt-<t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="html5:dfn/@doc:en | html5:i/@doc:en | doc:iw/@en">
    <t:attribute name="title"><t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="child::html5:dfn/@doc:key"/>
  
  <t:template match="child::doc:iw | child::html5:i">
    <t:variable name="dfn" select="/descendant::html5:dfn[(string (@doc:name) = string (current ()/self::doc:iw/@name | current ()/self::html5:i/@doc:name)) or (string (self::*) = string (current ()))] [position () = 1]"/>
    <i>
      <t:apply-templates select="@*"/>
      <t:if test="self::doc:iw and not (@xml:id)">
        <t:attribute name="id">
          <t:value-of select="'index-'"/>
          <t:value-of select="generate-id (self::node ())"/>
        </t:attribute>
      </t:if>
      <t:choose>
      <t:when test="$dfn and not (ancestor::doc:markRef)">
        <a href="#dt-{$dfn/@doc:name}" rel="glossary">
          <t:apply-templates/>
        </a>
      </t:when>
      <t:otherwise>
        <t:apply-templates/>
      </t:otherwise>
      </t:choose>
    </i>
  </t:template>
  
  <t:template match="child::html5:i" mode="text">
    <i>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </i>
  </t:template>
  
  <t:template match="child::html5:i" mode="attr">
    <t:apply-templates mode="attr"/>
  </t:template>
  
  <t:template match="child::html5:i/@doc:name"/>
  <t:template match="child::doc:iw/@name"/>
  <t:template match="child::doc:iw/@key"/>
  
  <t:template match="child::doc:iref[@section]">
    <t:variable name="target"
        select="(/descendant::html5:section | /descendant::doc:appendix |
                 /descendant::doc:presection)
                   [string (@xml:id) = string (current ()/@section)]"/>
    <a class="doc-iref-section">
      <t:if test="$target">
        <t:attribute name="href">#<t:value-of select="@section"/></t:attribute>
      </t:if>
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:attribute name="title">
          <t:value-of select="$target/child::xhtml2:h"/>
        </t:attribute>
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <t:for-each select="$target">
          <t:choose>
          <t:when test="$target/ancestor-or-self::doc:appendix">
            <t:number count="doc:appendix" format="$B!x(BA"/>
            <t:if test="$target/self::html5:section">
              <t:number level="multiple" count="html5:section" format=".1"/>
            </t:if>
            <t:value-of select="' '"/>
          </t:when>
          <t:when test="$target/ancestor-or-self::doc:presection"/>
          <t:otherwise>
            <t:number level="multiple" count="html5:section" format="$B!x(B1.1 "/>
          </t:otherwise>
          </t:choose>
          <cite class="section"><t:value-of select="child::xhtml2:h"/></cite>
        </t:for-each>
      </t:otherwise>
      </t:choose>
    </a>
  </t:template>
  <t:template match="doc:iref[@section]/@section"/>
  
  <t:template match="child::doc:iref[@fig]">
    <t:variable name="target"
        select="/descendant::doc:fig
                   [string (@xml:id) = string (current ()/@fig)]"/>
    <a class="doc-iref-fig">
      <t:if test="$target">
        <t:attribute name="href">#<t:value-of select="@fig"/></t:attribute>
      </t:if>
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:attribute name="title">
          <t:apply-templates select="$target/child::doc:caption" mode="attr"/>
        </t:attribute>
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <t:apply-templates select="$target/child::doc:caption" mode="text"/>
      </t:otherwise>
      </t:choose>
    </a>
  </t:template>
  <t:template match="doc:iref[@fig]/@fig"/>
  
  
  <t:template match="child::doc:iref[@quote]">
    <t:variable name="target"
        select="/descendant::doc:quote
                   [string (@xml:id) = string (current ()/@quote)]"/>
    <a class="doc-iref-quote">
      <t:if test="$target">
        <t:attribute name="href">#<t:value-of select="@quote"/></t:attribute>
      </t:if>
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:attribute name="title">
          <t:apply-templates select="$target/child::doc:quoteCredit" mode="attr"/>
        </t:attribute>
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <t:apply-templates select="$target" mode="figure-number"/>
        <t:apply-templates select="$target/child::doc:quoteCredit" mode="text"/>
      </t:otherwise>
      </t:choose>
    </a>
  </t:template>
  <t:template match="doc:iref[@quote]/@quote"/>
  
  <t:template match="doc:quote" mode="figure-number"/>
  
  <t:template match="child::doc:iref[@problem]">
    <t:variable name="target"
        select="/descendant::doc:problem
                   [string (@xml:id) = string (current ()/@problem)]"/>
    <a class="doc-iref-problem">
      <t:if test="$target">
        <t:attribute name="href">#<t:value-of select="@problem"/></t:attribute>
      </t:if>
      <t:apply-templates select="@*"/>
      <t:choose>
      <t:when test="child::node ()">
        <t:attribute name="title">
          <t:value-of select="'$BLd(B'"/>
          <t:for-each select="$target">
            <t:number count="doc:problem"/>
          </t:for-each>
        </t:attribute>
        <t:apply-templates/>
      </t:when>
      <t:otherwise>
        <span class="section" lang="ja" xml:lang="ja">
          <t:value-of select="'$BLd(B'"/>
          <t:for-each select="$target">
            <t:number count="doc:problem"/>
          </t:for-each>
        </span>
      </t:otherwise>
      </t:choose>
    </a>
  </t:template>
  <t:template match="doc:iref[@problem]/@problem"/>
  
  <t:template match="child::html5:em | child::html5:q |
                     child::html5:code | child::html5:var |
                     child::html5:abbr | child::html5:span |
                     child::xhtml1:rb | child::xhtml1:rt |
                     child::html5:blockquote">
    <t:element namespace="{namespace-uri ()}" name="{local-name ()}">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </t:element>
  </t:template>
  
  <t:template match="child::html5:abbr |
                     child::html5:code" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::html5:abbr" mode="attr">
    <t:apply-templates mode="attr"/>
  </t:template>
  
  <t:template match="child::html5:em" mode="text">
    <t:apply-templates/>
  </t:template>
  
  <t:template match="child::html5:m" mode="text">
    <t:apply-templates/>
  </t:template>
  
  <t:template match="child::html5:q" mode="text">
    <q>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </q>
  </t:template>
  
  <t:template match="child::xhtml1:ruby">
    <ruby>
      <t:apply-templates select="@*"/>
      <t:apply-templates select="child::xhtml1:rb"/>
      <rp> (</rp>
      <t:apply-templates select="child::xhtml1:rt[position () = 1]"/>
      <t:if test="child::xhtml1:rt[position () = 2]">
        <rp> / </rp>
        <t:apply-templates select="child::xhtml1:rt[position () = 2]"/>
      </t:if>
      <rp>) </rp>
    </ruby>
  </t:template>
  
  <t:template match="child::xhtml1:ruby" mode="text">
    <t:apply-templates select="(child::xhtml1:rb |
                                child::xhtml1:rbc/child::xhtml1:rb)/child::node ()"/>
  </t:template>
  
  <t:template match="html5:abbr/@title">
    <t:attribute name="title"><t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="child::html5:m">
    <em class="m">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </em>
  </t:template>
  
  <t:template match="child::doc:tag">
    <code class="tutorial-tag">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  
  <t:template match="child::doc:declaration">
    <code class="HTML doc-sgml-declaration">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  
  <t:template match="child::doc:lang-tag">
    <code class="lang">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  <t:template match="child::doc:lang-tag" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::doc:uri">
    <code class="uri">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  <t:template match="child::doc:uri" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="html5:a[@href]">
    <a href="{string (@href)}">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </a>
  </t:template>
  <t:template match="html5:a/@href"/>
  
  <t:template match="child::doc:weak">
    <span class="weak">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </span>
  </t:template>
  <t:template match="child::doc:weak" mode="text"/>
  <t:template match="child::doc:weak" mode="attr"/>
  
  <t:template match="child::dc:title">
    <cite class="dc-title">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </cite>
  </t:template>
  <t:template match="child::dc:title" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::dc:title" mode="attr">
    <t:value-of select="'$B!X(B'"/>
    <t:apply-templates mode="attr"/>
    <t:value-of select="'$B!Y(B'"/>
  </t:template>
  
  <t:template match="child::dc:creator">
    <cite class="dc-creator">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </cite>
  </t:template>
  <t:template match="child::dc:creator" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::dc:creator" mode="attr">
    <t:apply-templates mode="attr"/>
  </t:template>
  
  <t:template match="child::doc:htmlCode">
    <code class="HTML">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  
  <t:template match="doc:figBody/child::doc:htmlCode |
                     doc:answer/child::doc:htmlCode |
                     doc:htmlCode[@display = 'block']">
    <pre><code class="HTML">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code></pre>
  </t:template>
  <t:template match="doc:htmlCode/@display[string (self::node ()) = 'block']"/>
  
  <t:template match="child::doc:cssCode">
    <code class="CSS">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code>
  </t:template>
  
  <t:template match="doc:figBody/child::doc:cssCode |
                     doc:answer/child::doc:cssCode">
    <pre><code class="CSS">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </code></pre>
  </t:template>
  
  <t:template match="child::doc:unit-number">
    <span class="unit-number">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </span>
  </t:template>
  
  <t:template match="child::doc:number">
    <span class="number">
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </span>
  </t:template>
  
  <t:template match="child::doc:unit">
    <t:choose>
    <t:when test="string (self::node ()) = 'mm'">
      <abbr class="unit" title="millimeter" lang="en" xml:lang="en">
        <t:apply-templates select="@*"/>
        <t:apply-templates/>
      </abbr>
    </t:when>
    <t:when test="string (self::node ()) = 'cm'">
      <abbr class="unit" title="centimeter" lang="en" xml:lang="en">
        <t:apply-templates select="@*"/>
        <t:apply-templates/>
      </abbr>
    </t:when>
    <t:otherwise>
      <span class="unit">
        <t:apply-templates select="@*"/>
        <t:apply-templates/>
      </span>
    </t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template match="doc:markRef">
    <t:variable name="mark"
        select="/descendant::*[@doc:markName = current ()/@mark]"/>
    <a class="doc-mark-ref" href="#mark-{generate-id ($mark)}">
      <t:attribute name="onmouseover">
        <t:for-each select="$mark">
          document.getElementById ('mark-<t:value-of 
          select="generate-id (self::node ())"
          />').className += ' doc-mark-enabled';
        </t:for-each>
      </t:attribute>
      <t:attribute name="onmouseout">
        var el;
        <t:for-each select="$mark">
          el = document.getElementById ('mark-<t:value-of select="generate-id 
          (self::node ())" />');
          el.className = el.className.replace (/\s*\bdoc-mark-enabled\b/g, '');
          <!-- TODO: Clones -->
        </t:for-each>
      </t:attribute>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
    </a>
  </t:template>
  <t:template match="doc:markRef/@mark"/>
  
  <t:template match="@doc:markName">
    <t:attribute name="id">
      <t:value-of select="concat ('mark-', generate-id (parent::node ()))"/>
    </t:attribute>
  </t:template>
  
  <t:template match="@xml:lang">
    <t:apply-templates select="parent::*" mode="lang"/>
  </t:template>
  
  <t:template match="@xml:lang" mode="text">
    <t:apply-templates select="self::node ()"/>
  </t:template>
  
  <t:template match="child::*" mode="lang">
    <t:choose>
    <t:when test="@xml:lang">
      <t:attribute name="lang"><t:value-of select="@xml:lang"/></t:attribute>
      <t:attribute name="xml:lang"><t:value-of select="@xml:lang"/></t:attribute>
    </t:when>
    <t:otherwise>
      <t:attribute name="lang">
        <t:value-of select="ancestor::*[@xml:lang]/@xml:lang"/>
      </t:attribute>
      <t:attribute name="xml:lang">
        <t:value-of select="ancestor::*[@xml:lang]/@xml:lang"/>
      </t:attribute>
    </t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template match="html5:*/@class">
    <t:attribute name="class"><t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="@xml:id">
    <t:attribute name="id"><t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="@xml:space">
    <t:attribute name="xml:space"><t:value-of select="self::node ()"/></t:attribute>
  </t:template>
  
  <t:template match="text ()" mode="attr">
    <t:value-of select="normalize-space (self::node ())"/>
  </t:template>
  
  <t:template match="child::*" mode="unknown">
    <span>
      <code>
        <t:value-of select="concat ('{&lt;', namespace-uri (), '>:',
                                    local-name (), '}')"/>
      </code>
      <t:apply-templates select="@*"/>
      <t:apply-templates/>
      <code>
        <t:value-of select="'{/}'"/>
      </code>
    </span>
  </t:template>
  <t:template match="child::*">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="h1">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="h2">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="h3">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="answer">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="text">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc-figures">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc-value">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc-detail">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc-columns">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="toc-keypoints">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="figure-number">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="figure-number-attr">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="child::*" mode="todo-list">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  
  <t:template match="child::*" mode="attr">
    <t:value-of select="concat ('{&lt;', namespace-uri (), '>:',
                                local-name (), '}')"/>
    <t:apply-templates mode="attr"/>
    <t:value-of select="'{/}'"/>
  </t:template>
  
  <t:template match="@*" mode="unknown">
    <span>
      <code>
        <t:value-of select="concat ('{@&lt;', namespace-uri (), '>:',
                                    local-name (), '=')"/>
      </code>
      <t:value-of select="string (self::node ())"/>
      <code>
        <t:value-of select="'/}'"/>
      </code>
    </span>
  </t:template>
  <t:template match="@*">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
  <t:template match="@*" mode="text">
    <t:apply-templates select="self::node ()" mode="unknown"/>
  </t:template>
</t:stylesheet>
<!--
  License: GNU FDL (See |webpage.ja.html|).
  $Date: 2006/03/07 06:23:49 $
-->
