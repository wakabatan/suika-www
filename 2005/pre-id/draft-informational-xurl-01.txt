Draft of a RFC (Informational Protocol)                   April 16, 1996
Category: Informational
                                                        Tsuyoshi Hayashi
                                                      Barrier Free, Inc.


                Extended Uniform Resource Locator (XURL)
                    <draft-informational-xurl-01.txt>


Status Of This Memo

   This memo provides information for the Internet community.  This memo
   does not specify an Internet standard of any kind.  Distribution of
   this memo is unlimited.


Abstract

   This document proposes an Extended Uniform Resource Locator (XURL), a
   compact string representation for a resource both available via the
   Internet and not.

   By using the XURL, we can indicate any types of resources and also
   integrate the "two" worlds, the Internet world and the real world.
   The XURL scheme is a extended version of the Uniform Resource Locator
   (URL).


0. About "Resource"

   In this memo, "resource" means any type of things, matters, concepts,
   or spirituals; a lot of fact information, databases, computer files,
   paper files, hardwares, softwares, both online and offline
   information services, shops, foods, animals or flowers, human beings
   (including you or I), atoms, genomes, good or bad ideas, devices,
   feeling, Genki mind or something else.  Something are on the
   Internet, something are around you or me.


1. Introduction

   Uniform Resource Locator (URL) is a very useful code for the Internet
   community.  By using this code, we can indicate (point out) a lot of
   resources (computer files or services) provided on the Internet.
   But URLs are only effective in the Internet field.

   Although there are a lot of resources outside the Internet, we cannot
   indicate or identify them from the Internet side.  In a few years,
   more information services will be provided on the Internet.  But, of
   course, many resources will be still in the real world, and probabry
   something will be both.

   After all, we have to handle two different-type resources; one is
   resources on the Internet, another is resouces outside the Internet.
   We know how to handle the former today, but we have no way to handle
   the latter yet.  Therefore, I think we need the XURL in order to both
   indicate resources outside the Internet and integrate the two worlds.

   You can use XURL anytime you want to indicate resources inside or
   outside the Internet world.  For example, you can indicate and/or
   trace a copyright-holding object (which is on the Internet or not) by
   the XURL.  Any data on any Internet servers or any papers someone
   wrote are the same.

   By using the XURL, maybe we may be able to break the barrier between
   the Internet world and the Real world.  In addition, the XURL has a
   function for the digital timestamp service if appropriate
   organizations provide digital siging and notarizing systems.


2. Description

   If you would like to indicate a resource on the Internet, you can use
   both URL [URL] and XRUL.  On the other hand, if you would like to
   indicate a resource outside the Internet, you have to use this XURL
   scheme in following format.

2.1 General XURL Syntax

   A full BNF description of the XURL syntax is not given in this
   document.  Sorry.

   In general, XURLs are written as follows:

       X<scheme>.<timestamp>.<scheme-specific-part>

   An XURL starts with a letter either "X" or "x".  (A uppercase of "X"
   is recommended.)  Next, the XURL contains the name of the scheme
   being used (<scheme>) followed by a period, the timestamp being used
   (<timestamp>) followed by a period, and then a string (the <scheme-
   specific-part>) whose interpretation depends on the scheme.

   Scheme names consist of a sequence of characters. The allowed letters
   are depended on the scheme.  For resiliency, systems interpreting
   XURLs should treat upper case letters as equivalent to lower case in
   scheme names (e.g., allow "REAL" or "Real" as well as "real").

2.2 Usable Character Set for XURLs

   XURLs are sequences of characters, i.e., letters, digits, and special
   characters. An XURLs may be represented in a variety of ways: e.g.,
   ink on paper (printed on paper), data in computer file, or one's
   memory.

   XURLs are written only with the graphic printable characters of the
   US-ASCII [ASCII] coded character set. The octets 80-FF hexadecimal
   are not used in US-ASCII, and the octets 00-1F and 7F hexadecimal
   represent control characters; these must be encoded.

   If you would like to use some non-US-ASCII character set in the XURL,
   it must be encoded within printable US-ASCII character set.  For
   example, if you want to use a Japanese Kanji characters set, it must
   be encoded by ISO-2022-JP [ISO2022] or the other appropriate ways.

2.3 Specific Schemes

   The schemes covered are:

     inet       for resources on the Internet
     inet+      for resources not provided on the Internet yet
     real       for resources exist in the real world
     feel       for resources held in one's heart
     misc       for unknown things

2.4 Timestamp Format and Meaning

   The timestamp consistes of following parts:

     <date>     like "19950415" or "15Apr1996" (means April 15, 1996)
     @<time>    like "@2315" (means at 11:15 p.m.)
     <timezone> like "+0900" or "JST" (means Japanese Standard Time)

   Some or all of the parts "<date>", "@<time>", "<timezone>" may be
   excluded.  If all part of the timestamp are shown, the XURL can be
   notarized by appropriate organization(s).

2.5 How to Indicate Resources on the Internet

   If you want to indicate some resource on the Internet, you can use
   following XURL form:

     XURL: Xinet.<timestamp>.<url-expression>

   The <url-expression> must conform with the RFC 1738 [URL] and the
   others URL-related RFCs ([URI] and [URN]).  The "<timestamp>" is used
   in order to fix the data provided on the Internet actually (and
   certainly) at the specified the date/time/timezone.

   If you don't use "<timestamp>", you should better to use URLs, not
   XURLs.

2.6 How to Indicate Resources Exist in the Real World

   If you want to indicate some resource outside the Internet, you can
   use following XURL form:

     XURL: Xreal.<timestamp>.<scheme-specific-part>

   You can also use following style instead of above as a short form:

     XURL: X.<timestamp>.<scheme-specific-part>

   This XURL indicates something exist in our real world.  The
   "<timestamp>" is used in order to fix the matter or fact.

   Note that the timestamp part is a optional.

2.7. How to Indicate Resources Held in One's Heart

   If you want to indicate some resource held in one's heart, you can
   use following XURL for representation of it:

     XURL: Xfeel.<timestamp>.<scheme-specific-part>

   This XURL indicates something in spiritual domain.  The "<timestamp>"
   is used in order to fix when one felt so.  For example, someone can
   record or describe one's deep mind within this XURL as one writes
   down one's word on one's daily.

   Note that the timestamp part is a optional.

2.8. A Guideline of scheme-specific-part

   A rough guideline to describe scheme-specific-part part is a follwing
   form:

     <category>:<method>//<target>/

   where

     <category> category of the "<target>"
     <method>   way to indicate the "<target>"
     <target>   things what you want to represent.

   The "<method>" may be excluded.  If "<category>" is depend on
   "<method>", "<category>" may be excluded.


3. Examples

   I can not show enough or typically examples because "<scheme-
   specific-part>" part allows various ways for describing resources.
   But, for explanation, this memo exhibits some examples.

Case 1a: a newspaper

     XURL: Xreal.19950415.newspaper://NYT/1/
     XURL: Xreal.15Apr1996.newspaper://NYT/1/
     XURL: X.15Apr1996.newspaper://NYT/1/

     Meaning: The top page of the New York Times of April 15, 1996.

Case 1b: a article of the newspaper

     XURL: X.19950413JST.newspaper:xy//Nikkei/1/x170y30/

     Meaning: A article printed on the newspaper at the position P(x,y)
        is (170,30) on the top page of the Nikkei Shinbun (printed by
        nikkei.co.jp) of April 13, 1996.  Where the (170,30) pair
        indicates the absolute position of the article on the page.  The
        Unit of each value (x or y) is described in a millimeter.  The
        origin of axis is always left-top of each page.

        Actually, there is the article that the U.S. Gov. will propose
        the international rules for distribution on the Internet at the
        next round of the World Trade Organization (WTO, WTO.ORG).  So,
        if NIKKEI.CO.JP will provide a appropriate sequence of
        characters only for the article, we will be able to point out
        the article by following XURLs (though this is a only sample,
        not valid now):

     XURL: X.19950413JST.newspaper:keywords//Nikkei/1
        /international&internet&distribution&wto&gus-gov&copyright
     XURL: X.19950413JST.newspaper:keywords//Nikkei/1
        /International_rules_for_distribution/Internet/Keep_copyright
        /USGov_will_propose/

Case 2a: a book:

     XURL: Xreal..book:isbn//1-56592-098-8/

     Meaning: A book numbered "1-56592-098-8" in the International
        Standard Book Number (ISBN) code.  Actually, the title of this
        book is "PGP: Pretty Good Privacy" so this book is also
        indicated in following optional form:

     XURL: Xreal..book:isbn+title//1-56592-098-8
        /"PGP:_Pretty_Good_Privacy"/

        Each underbar ('_') should be replace with a blank (' ') when
        you decode XURL into a normal sequence of characters.  If a book
        is not numbered in ISBN, this memo offers a following form:

     XURL: Xreal..book:title//"A_History_of_Tsuyoshi_Hayashi"/
     XURL: Xfeel..vbook:title//"A_History_of_Tsuyoshi_Hayashi"/

        The last XURL shows a virtual book titled "A History of Tsuyoshi
        Hayashi", which is only exist in one's heart.  See Case 16.

Case 2b: a software distributed as a book:

     XURL: Xreal..soft:isbn//4-88734-301-9/

     Meaning: A software shipped book style numbered "4-88734-301-9" in
        the ISBN.  Actually, this is a computer software (picture date)
        packed in two floppy disks (FDs).

Case 3a: a record (audio CD)

     XURL: Xreal.1980.cd_a:dns+//windham.com/WH1012CD12.98/

     Meaning: A audio CD (compact disk) numbered "WH1012CD12.98" (this
        code may be depended by the record company) released by
        windham.com (Windham Hill Records) in 1980.  The CD is titled
        "George Winston  AUTUMN" so that we can describe it in following
        form:

     XURL: Xreal.1980.cd_a:dns+//windham.com/"George_Winston"/AUTUMN/

Case 3b: a music

     XURL: Xreal.1980.cd_a:dns+//windham.com/"George_Winston"/AUTUMN
        /Moon/

     Meaning: The 5th music named "Moon" of XURL:Xreal.1980.cd_a:dns+
        //windham.com/"George_Winston"/AUTUMN/.  In actually, we can
        listen the sound at following XURL:

     XURL: Xinet.19960415@0535+0900.http://www.windham.com/audio
        /winston_autumn.au

Case 4: a service of package distributer

     XURL: Xreal.24Dec1995@1800.pkg:depttime//UPS.COM/Mary/John/

     Meaning: A package sent from John to Mary on the evening (18:00) of
        the Xmas Eve. ;-)

   If the XURL of this case will be used in effectively, the package
   distributer will be able to provide some good services for their
   customers.

Case 5a: a city (or a place)

     XURL: Xreal..place:dns//city.yokohama.jp/

     Meaning: A city of Yokohama in Japan.  Yokohama city got a
        domainname, city.yokohama.jp, from JPNIC [JPNIC].

     XURL: Xreal..place:dns+snail//city.yokohama.jp/235/Isogo-ku
        /Nakahara/2-13-19-201/

     Meaning: The XURL shows following snail (postal) address of
        "2-13-19-201 Nakahara, Isogo-ku, Yokohama 235, Japan".

Case 5b: a station

     XURL: Xreal..station:char//JR-E/Tokaido/yokomaha/

     Meaning: A Yokohama station of Tokaido line managed by Japan
        Railway East (JR-E).  If someone knows the geometry of the
        Yokohama station, someone can represent there in another way:

     XURL: Xreal..station:geom//NL35.278/EL139.375/

        which shows a station placed in lat. 35 deg. 278' North and in
        long. 139 deg. East.

Case 6: a phone or fax number:

     XURL: Xreal..phone:ITU//+81-45-776-3524/
     XURL: Xreal..fax:ITU//+81-45-776-3524/

     Meaning: A phone of fax number, "+81 45 776-3524", in a style used
        in the the International Telecommunication Union (ITU).

     XURL: Xreal..phone:local//JP/045-776-3524/

     Meaning: A phone number, "045-776-3524", in a local style of Japan.
        (By the way, you can get the current Japanese Standard Time
        (JST) in following XURL:

     XURL: Xreal..phone:ITU//+81-117/

Case 7: a person:

     XURL: Xreal..person:email//take@barrier-free.co.jp/

     Meaning: A person whose email address is "take@barrier-free.co.jp".

     XURL: Xreal..person:email+//take@barrier-free.co.jp
        /"Tsuyoshi_Hayashi"/

     Meaning: A person whose email address is "take@barrier-free.co.jp"
        and whose name is Tsuyoshi Hayashi.

     XURL: Xreal..person:dns+snail//city.yokohama.jp/235/Isogo-ku
        /Nakahara/2-13-19-201/"Tsuyoshi_Hayashi"/

     Meaning: A person whose address is "2-13-19-201 Nakahara, Isogo-ku,
        Yokohama 235, Japan" and whose name is Tsuyoshi Hayashi.

     XURL: Xreal..person:credit_n//visa/0202-0217-0139-0000/

     Meaning: A person who has a VISA credit card and the card number is
        "0202-0217-0139-0000".  (Note that this credit card number,
        0202-0217-0139-0000, is a complete fiction.)

Case 8: a network or a host on the Internet:

     XURL: Xreal..networks:ipaddr//202.217.139.0/
     XURL: Xreal..networks:whois//BF-Net/

     Meaning: A network on the Internet assigned 202.217.139.0 (1st
        XURL) or named BF-Net (2nd XURL).

     XURL: Xreal..hosts:ipaddr//202.217.139.5/
     XURL: Xreal..hosts:dns//www.barrier-free.co.jp/

     Meaning: A host on the Internet assigned 202.217.139.5 (3rd XURL)
        or named www.barrier-free.co.jp (4th).

   Note that IP addresses and domainnames are "real" public resources as
        same as phone numbers so that XURLs can indicate such resources
        within the XURL scheme.

Case 9: a part of one's body:

     XURL: Xreal..body:email+//take@barrier-free.co.jp/eye/right/
     XURL: Xreal..inner:email+//take@barrier-free.co.jp/marrow/
     XURL: Xreal..blood:email+//take@barrier-free.co.jp/A_RH+/

     Meaning: From 1st to 3rd, these XURL indicates a part of the body
        of a person whose email address is "take@barrier-free.co.jp".
        1st indicates the person's right eye.  2nd indicates the
        person's marrow.  3rd indicates that the blood type of the
        person is "A (RH+)".

   If real hospitals, eye banks, marrow banks or blood banks will use
   these XURLs, some lives may not be lost...

Case 10: a genome

     XURL: Xreal..geneticmap://human/21q22.2/

     Meaning: The XURL indicates a human genome region numbered 21q22.2,
        "q22.2" part of 21th chromosome.  This part is called "down
        syndrome critical region" [GENOMU].

Case 11: a fact information

     XURL: Xreal..fact:period//JP/1941/1945/World_War_2/
     XURL: Xreal..fact:period//US/1941/1945/World_War_2/

     Meaning: Long long days ago, in 1941, many nations go to the 2nd
        War.  The War was terminated in 1945.  These XURLs describe a
        period of the history of Japan and United Status.  On the other
        hand, following XURL indicates the war related with the
        Internet:

     XURL: Xreal.8Feb1996.fact://US/CDA/signed/

        where CDA is the Communications Decensy Act of 1996.

   This is usable format for the press or the government.

Case 12: a drink or food (with a bar code):

     XURL: Xreal..drink:JAN//4-901411-17661-1/

     Meaning: Something labeled "4-901411-17661-1" in Japanese Article
        Number (JAN).  In fact, this is a beer (filled in a aluminum
        can) brewed by KIRIN, a famouse Japanese brewery company.
        (Sometimes I drink it. ;-))  JAN is one of the most used bar
        code symbol in Japan.  This symbol in same class is used in the
        United States in another name, U.P.C.

   Note that the bar code symbol is a graphical mark.  A single symbol
   is created by some narrow vertical bars like this (sorry, a bad
   sample...):

        |  || | ||| || ||||| ||   | ||| | |||   |

   The XURL in this case will be used probabry in Electronic Date
   Interchange (EDI) or the same domain.  The other bar code named
   "ISBT128" was defined and used by American Association of Blood
   Banks [AIMJ].  See Case 9.

   (Note that there are several type of bar code format; CODE39,
   CODE128, CODEBAR (NW-7), 2of5 (ITF), PDF417 and more.  I think that
   bar codes are very useful in order to combine several information
   with us.  Because the bar code symbol (which printed on somewhere) is
   the offline devide but it is computer-readable information.
   Especially, PDF417 can hold more than 1024 bytes of data in a single
   machine-readable symbol printed on paper, and the symbology encodes
   full US-ASCII [ASCII] or binary data (octets 00-FF hexadecimal) so
   that it is suitable for the Internet field, I think.

   In addition, this is a fun for me, also good for the Internet
   community, Symbol Technologies, which developed and patented PDF417,
   placed PDF417 in the public domain, making it free from any use
   restriction, licenses and fees.

   For more exact information, see the U.S.S issued by AIM [AIMJ] or
   read the press release about PDF417 [SYMBOL].)

Case 13: money (currency)

     XURL: Xreal..money:print//US/dollar/199.95/
     XURL: Xreal..money:print//JP/yen/4,980/
     XURL: Xinet+..money:ec//TheNet/DigitalMoney/1234/

     Meaning: The 1st means US$ of 199 and 95 cents.  The 2nd means
        Japanese Yen of 4,980.  A "print" means both printed bills and
        coins, i.e., real money.  On the other hand, the 3rd XURL
        indicates electronic (digital) cash or other digital technology-
        based money systems.

   These are usable format for the banks.  Note that there is a good
   (but difficult) paper [EMONEY] for understanding a electronic money
   system written by Citibank, N.A., one of the most famous U.S. bank.

Case 14: a structure of the organization

     XURL: Xreal..org:dns+//MITI.GO.JP/Industrial_Policy_Bureau
        /Deputy_Director-General/Research_Division/

     Meaning: A division named "Research Division" of the Deputy
        Director-General division of the Industrial Policy Bureau of the
        MITI.GO.JP, the Japanese Government's Ministry of International
        Trade and Industry (MITI) [MITI].

   This case is usable for the most government agencies, international
   organizations, foundations, companies (especially for a personnel
   depatment of the company), universities and fimilies.  This may be
   also usable for chemistry formulas.

Case 15: a law

     XURL: Xreal..law:n//US/5-USC-552/
     XURL: Xreal.8Feb1996.law:n+stat//US/S.652/signed/
     XURL: Xinet.19960412.http://rs9.loc.gov/cgi-bin/query
        /z?c104:S.652.ENR:
     XURL: Xreal.19461103.law:jp_roman//JP/Nippon_koku_Kenpou
        /promulgated/

     Meaning: 1st XURL indicates a U.S. act which is numbered "5 U.S.C.
        Section 552".  The title of the act is the "Freedom of
        Information Act (FOIA)".  The 2nd XURL means S.652, the
        Telecommunications Act of 1996, and the act was signed on Feb.
        8, 1996.  The 3rd means a HTML version of S.652 was provided at

          URL: http://rs9.loc.gov/cgi-bin/query/z?c104:S.652.ENR:

        on Apr. 12, 1996 certainly.

        The last, 4th means the Constitution of Japan was promulgated
        on Nov. 3, 1946.  A "Nippon koku Kenpou" is represented in
        Japanese-Roman rule.  There are no official numbering system for
        each laws in Japan so that I could not point out the
        Constitution of Japan in easy way.

Case 16: one's mind

     XURL: Xfeel.24Dec1995@1800.mind:words//John_loves_Mary/
     XURL: Xfeel.19960416@0545JST.mind:words//I_am_very_tired/

     Meaning: The 1st XURL means that at 18:00 on December 24 in 1995,
        John certainly loves Mary.  The 2nd XURL means that at 05:45 of
        today, "I am very tired now" because I have been writing this
        document for a long hours...  And following 3rd XURL

     XURL: Xfeel.19960125JST.mind:email/take@barrier-free.co.jp
        /is/Genki/

        means that a person whose email address is
        "take@barrier-free.co.jp" was Genki (at least) on 25 Jan, 1996.
        Note that a "Genki" is a Japanese word which means bright,
        active, fresh, smily, and straight.  I shall be so.

   In this way, XURL can indicate any kind of things both material and
   non material; both digital information and analog one.


4. Building the Official Rules for XURL

   There is no official rules for describing XURLs.  Each part of XURL
   syntax should be defined by a independent orgnazation like the
   Internet Assigned Numbers Authority (IANA).  In addition, I hope that
   a lot of people send me good sugestions or questions for building
   the best useful rules for all future users of the XURL


5. XURL Using Policy: Effectively and Harmoniously

   If XURL is useful for both the Internet and the real community, I
   hope that all people, companies, organizations use it effectively and
   harmoniously.  I do not allow anyone occupies the profit/benefit
   result from this memo.

   If this memo will be published as a Informational RFC, this memo will
   be dedicated to the Internet community and the future.


6. Pretty Good Naming

   I wish to call this useful compact string XURL.  I am very pleased
   with this word.  But this may be a bad naming; someone may confuse
   "XURL" with "URL".  So I thought the other namings for this:

    (a) Integrated Resource Identifier (IRI),
    (b) Integrated Resource Handling System (IRHS).

   Is this a pretty good naming?  Or, do you know another good one?


7. Security Considerations

   To keep the timestamp information from unfair or unlawful tamperings,
   we should install some digital signature technologies like MD5 [MD5].
   So I will propose additional XURLs form like this:

     XURL: Xreal.8Feb1996.law:+md5//US/S.652/<md5-string>/

   where <md5-string> is a string in hexdecimal of 128-bit MD5 encoded
   hash data for the original text file of the S.652 like this:

     900150983cd24fb0d6963f7d28e17f70

   In addition, some people probably need encrypting considerations.  So
   we should better to define the following forms:

     XURL: X..cd_rom:des+rsa//locallocker.WhiteHouse
        /some_secretive_documents_for_the_Govrnment/
     XURL: Xinet.pubkey@pgp.http://www.barrier-free.co.jp/take/pgpkey


8. Copyright Notice

   All of company names, product names, service names and other same
   class names may be trademarks or registered trademarks of each
   companies, organizations or else.

   Althogh it may have some problems because of using these names above
   in this memo, I did use them.  Because I think that this is a good
   way to understand for many people who will get this memo.

   If few companies will make a objection about this memo, I will have
   to exchange current example(s) for another example(s), and will
   expire this memo and write next version of it.


9. References or Concerns

   [AIMJ] AIM Japan, "Uniform Symbology Specification (USS)",
   distributed on 4 October, 1995. <See also: URL:http://www.aimusa.org>

   [ASCII] "Coded Character Set -- 7-bit American Standard Code for
   Information Interchange", ANSI X3.4-1986.

   [EMONEY] Citybank, N.A., "A Electronic Money System", a official
   patent notice by the Japanese Patent Office as "TOKKYO KOHO number
   Heisei 7-111723" on November 29, 1995.

   [GENOMU] A memo (written by Tsuyoshi Hayashi) of a symposium titled
   "The Current Status of Human Genomu Analysis" held on 24 January,
   1996, Tokyo, Japan.

   [ISO2022] International Organization for Standardization (ISO),
   "Information processing -- ISO 7-bit and 8-bit coded character sets
   -- Code extension techniques", International Standard, Ref. No. ISO
   2022-1986 (E).

   [JPNIC] Japan Network Information Center (JPNIC), "About way to
   assign of reagn-type domain name". <URL:ftp://ftp.nic.ad.jp/pub
   /jpnic/domain-geographic.txt>

   [MD5] R. Rivest, "The MD5 Message-Digest Algorithm", IETF RFC 1321,
   April 1992.

   [MITI] The structure of INDUSTRIAL POLICY BUREAU of the MITI.
   <XURL:Xinet.19960415@1000JST.http://www.miti.go.jp/gsosikid.html>

   [SYMBOL] Symbol Technologies Inc., "PDF417 Uniform Symbology
   Specification Released". <XURL:Xinet.19960415@1700JST.http:
   //www.symbol.com/ST000011.HTM>

   [URI] T. Berners-Lee, "Universal Resource Identifiers in WWW", IETF
   RFC 1630, June 1994.

   [URL] T. Berners-Lee, L. Masinter, M. McCahill, "Uniform Resource
   Locators (URL)", IETF RFC 1738, December 1994.

   [URN] K. Sollins, L. Masinter, "Functional Requirements for Uniform
   Resource Names", IETF RFC 1737, December 1994.

10. Authors' Addresses

   Tsuyoshi Hayashi
   Barrier Free, Inc.
   2-13-19-201 Nakahara, Isogo-ku, Yokohama 235, Japan.
   Phone/Fax: +81-45-776-3524
   Email: take@barrier-free.co.jp
