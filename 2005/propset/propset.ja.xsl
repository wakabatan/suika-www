<?xml version="1.0" encoding="iso-2022-jp"?>
<t:stylesheet version="1.0"
    xmlns:t="http://www.w3.org/1999/XSL/Transform"
    xmlns:h1="http://www.w3.org/1999/xhtml"
    xmlns:h2="http://www.w3.org/2002/06/xhtml2">
  
  <t:variable name="source-language" select="'en'"/>
  
  <t:template match="/child::source">
    <t:processing-instruction name="xml-stylesheet">
      href="https://suika.suikawiki.org/www/style/html/xhtml" media="all"
    </t:processing-instruction>
    <t:processing-instruction name="xml-stylesheet">
      href="https://suika.suikawiki.org/www/style/ui/toc-none" media="all"
    </t:processing-instruction>
    <h2:html xml:lang="ja">
      <h2:head>
        <h2:title></h2:title>
      </h2:head>
      <h2:body>
        <t:apply-templates select="document (/child::source/child::uri)/child::*"/>
      </h2:body>
    </h2:html>
  </t:template>
  
  
  <t:template match="child::*">
    <h1:span style="display: block; color: red; border: dotted red thin;
                    padding: 1em; margin: 1em"
        xml:lang="{$source-language}">
      <t:value-of select="concat (local-name (), ': ')"/>
      <t:apply-templates select="child::* | attribute::* | text ()"/>
    </h1:span>
  </t:template>
  
  <t:template match="attribute::*">
    <h1:span style="display: block; color: red; border: dotted red thin;
                    padding: 1em; margin: 1em"
        xml:lang="{$source-language}">
      <t:value-of select="concat ('@', local-name (), ': ',
                                  string (self::node ()))"/>
    </h1:span>
  </t:template>
  
  <t:template match="child::propset">
      <h2:h>$BFC@-=89g(B</h2:h>
      <h2:dl>
        <t:choose>
          <t:when test="attribute::nsd">
            <h2:di><h2:dt>$B<oN`(B</h2:dt><h2:dd>$B0l<!FC@-=89g(B</h2:dd></h2:di>
            <h2:di><h2:dt>$B5-K!;EMMJ8=q(B</h2:dt>
              <h2:dd><t:call-template name="notation">
                       <t:with-param name="node" select="attribute::nsd"/>
                     </t:call-template></h2:dd></h2:di>
          </t:when>
          <t:otherwise>
            <h2:di><h2:dt>$B<oN`(B</h2:dt><h2:dd>$BJd=uFC@-=89g(B</h2:dd></h2:di>
          </t:otherwise>
        </t:choose>
        <h2:di><h2:dt>$B%0%m!<%V9=C[;EMMJ8=q(B</h2:dt>
          <h2:dd><t:choose>
                   <t:when test="attribute::gcsd">
                     <t:call-template name="notation">
                       <t:with-param name="node" select="attribute::gcsd"/>
                     </t:call-template>
                   </t:when>
                   <t:otherwise>
                     <t:call-template name="notation">
                       <t:with-param name="node" select="attribute::nsd"/>
                     </t:call-template>
                   </t:otherwise>
                 </t:choose></h2:dd></h2:di>
        <t:apply-templates select="child::desc | child::note"/>
        <t:call-template name="classdef-list"/>
        <t:call-template name="normdef-list"/>
        <t:call-template name="psmodule-list"/>
        <t:call-template name="classdef-list"/>
      </h2:dl>
      
      <t:apply-templates select="child::classdef | child::normdef | child::psmodule"/>
  </t:template>
  
  <t:template name="classdef-list">
        <t:if test="child::classdef">
          <h2:di>
            <h2:dt>$B5i(B</h2:dt>
            <h2:dd><h2:ul>
              <t:apply-templates select="child::classdef" mode="list"/>
            </h2:ul></h2:dd>
          </h2:di>
        </t:if>
  </t:template>
  
  <t:template name="propdef-list">
        <t:if test="child::propdef">
          <h2:di>
            <h2:dt>$BFC@-(B</h2:dt>
            <h2:dd><h2:ul>
              <t:apply-templates select="child::propdef" mode="list"/>
            </h2:ul></h2:dd>
          </h2:di>
        </t:if>
  </t:template>
  
  <t:template name="normdef-list">
        <t:if test="child::normdef">
          <h2:di>
            <h2:dt>$B@55,2=5,B'(B</h2:dt>
            <h2:dd><h2:ul>
              <t:apply-templates select="child::normdef" mode="list"/>
            </h2:ul></h2:dd>
          </h2:di>
        </t:if>
  </t:template>
  
  <t:template name="psmodule-list">
        <t:if test="child::psmodule">
          <h2:di>
            <h2:dt>$B%b%8%e!<%k(B</h2:dt>
            <h2:dd><h2:ul>
              <t:apply-templates select="child::psmodule" mode="list"/>
            </h2:ul></h2:dd>
          </h2:di>
        </t:if>
  </t:template>
  
  <t:template name="clause-notation">
    <t:choose>
      <t:when test="attribute::clausenot">
        <t:call-template name="notation">
          <t:with-param name="node" select="attribute::clausenot"/>
        </t:call-template>
      </t:when>
      <t:otherwise xml:lang="en">ISO/IEC 10744:1997 A.4.2</t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template match="child::when">
    <h2:di>
      <h2:dt>$BCMB8:_>r7o(B</h2:dt>
      <h2:dd xml:lang="{$source-language}"><t:apply-templates/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template match="child::desc">
    <h2:di>
      <h2:dt>$B@bL@(B</h2:dt>
      <h2:dd xml:lang="{$source-language}"><t:apply-templates/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template match="child::note">
    <h2:di>
      <h2:dt>$BCm0U(B</h2:dt>
      <h2:dd xml:lang="{$source-language}"><t:apply-templates/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="notation">
    <t:param name="node"/>
    <!-- XSLT 1.0 provides no access to notations -->
    <t:value-of select="concat ('(', $node, ')')"/>
  </t:template>
  
  <t:template mode="list" match="child::normdef | child::classdef |
                                 child::propdef | child::psmodule">
    <h2:li href="#{generate-id (self::node ())}"><t:call-template name="fullname"/>
           (<t:call-template name="rcsname"/>)</h2:li>
  </t:template>
  
  <t:template name="fullname">
    <h2:span xml:lang="{$source-language}">
      <t:choose>
        <t:when test="attribute::fullnm">
          <t:value-of select="attribute::fullnm"/>
        </t:when>
        <t:when test="attribute::appnm">
          <t:value-of select="attribute::appnm"/>
        </t:when>
        <t:otherwise>
          <t:value-of select="attribute::rcsnm"/>
        </t:otherwise>
      </t:choose>
    </h2:span>
  </t:template>
  
  <t:template name="appname">
    <t:choose>
      <t:when test="attribute::appnm">
        <h2:code xml:lang="{$source-language}"><t:value-of select="attribute::appnm"/></h2:code>
      </t:when>
      <t:otherwise>
        <h2:code xml:lang="{$source-language}"><t:value-of select="attribute::rcsnm"/></h2:code>
      </t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template name="rcsname">
    <h2:code xml:lang="{$source-language}"><t:value-of select="attribute::rcsnm"/></h2:code>
  </t:template>
  
  <t:template match="child::normdef">
    <h2:section><h1:a id="{generate-id (self::node ())}"/>
      <h2:h>$B@55,2=5,B'(B <t:call-template name="fullname"/></h2:h>
      <h2:dl>
        <t:call-template name="attributes-name"/>
        <t:call-template name="attributes-spec"/>
        <t:apply-templates select="child::desc | child::note"/>
      </h2:dl>
    </h2:section>
  </t:template>
  
  <t:template match="child::psmodule">
    <h2:section><h1:a id="{generate-id (self::node ())}"/>
      <h2:h>$B%b%8%e!<%k(B <t:call-template name="fullname"/></h2:h>
      <h2:dl>
        <t:call-template name="attributes-name"/>
        <t:call-template name="attribute-dependon"/>
        <t:call-template name="attribute-default"/>
        <t:apply-templates select="child::desc | child::note"/>
        <t:call-template name="classdef-list"/>
        <t:call-template name="propdef-list"/>
        <t:call-template name="normdef-list"/>
      </h2:dl>
      
      <t:apply-templates select="child::classdef | child::propdef | child::normdef"/>
    </h2:section>
  </t:template>
  
  <t:template match="child::classdef">
    <h2:section><h1:a id="{generate-id (self::node ())}"/>
      <h2:h>$B5i(B <t:call-template name="fullname"/></h2:h>
      <h2:dl>
        <t:call-template name="attributes-name"/>
        <t:call-template name="attributes-spec"/>
        <t:call-template name="attribute-default"/>
        <t:call-template name="attribute-prune"/>
        <t:call-template name="attribute-mayadd"/>
        <t:apply-templates select="child::desc | child::note"/>
        <t:apply-templates select="attribute::conprop | attribute::despprop"/>
        <t:call-template name="propdef-list"/>
      </h2:dl>
      
      <t:apply-templates select="child::propdef"/>
    </h2:section>
  </t:template>
  
  <t:template match="child::propdef">
    <h2:section><h1:a id="{generate-id (self::node ())}"/>
      <h2:h>$BFC@-(B <t:call-template name="fullname"/></h2:h>
      <h2:dl>
        <t:call-template name="attributes-name"/>
        <t:call-template name="attribute-cn"/>
        <t:call-template name="attributes-spec"/>
        <t:call-template name="attribute-default"/>
        <t:call-template name="attribute-vrfytype"/>
        <t:apply-templates select="attribute::datatype"/>
        <t:apply-templates select="child::when"/>
        <t:apply-templates select="child::desc | child::note"/>
      </h2:dl>
    </h2:section>
  </t:template>
  
  <t:template match="child::enumdef">
      <h2:dl><h1:a id="{generate-id (self::node ())}"/>
        <t:call-template name="attributes-name"/>
        <t:apply-templates select="child::desc | child::note"/>
      </h2:dl>
  </t:template>
  
  <t:template name="attributes-name">
    <h2:di>
      <h2:dt>$B;2>H6q>]9=J8L>(B</h2:dt>
      <h2:dd><t:call-template name="rcsname"/></h2:dd>
    </h2:di>
    <h2:di>
      <h2:dt>$B1~MQL>(B</h2:dt>
      <h2:dd><t:call-template name="appname"/></h2:dd>
    </h2:di>
    <h2:di>
      <h2:dt>$B40A4L>(B</h2:dt>
      <h2:dd><t:call-template name="fullname"/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attributes-spec">
    <h2:di>
      <h2:dt>$B;EMM=q(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="attribute::sd">
            <t:call-template name="notation">
              <t:with-param name="node" select="attribute::sd"/>
            </t:call-template>
          </t:when>
          <t:when test="ancestor::propset/attribute::nsd">
            <t:call-template name="notation">
              <t:with-param name="node" select="ancestor::propset/attribute::nsd"/>
            </t:call-template>
          </t:when>
          <t:otherwise>
            <t:call-template name="notation">
              <t:with-param name="node" select="ancestor::propset/attribute::gcsd"/>
            </t:call-template>
          </t:otherwise>
        </t:choose>
        
        <t:choose>
          <t:when test="attribute::clause">
            <t:value-of select="' '"/>
            <t:call-template name="clause"/>
          </t:when>
          <t:when test="self::propdef/parent::classdef/attribute::clause">
            <t:value-of select="' '"/>
            <t:for-each select="parent::node ()">
              <t:call-template name="clause"/>
            </t:for-each>
          </t:when>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="clause">
    <t:choose>
      <t:when test="attribute::clausenot">
        <t:value-of select="attribute::clause"/>
        [$B>r9`5-K!(B: <t:call-template name="clause-notation"/>]
      </t:when>
      <t:otherwise>
        <t:choose>
          <t:when test="contains (normalize-space (attribute::clause), ' ')">
            <t:value-of select="attribute::clause"/>
            [$B>r9`5-K!(B: ISO/IEC 10744:1997]
          </t:when>
          <t:when test="substring (attribute::clause, 1, 3) = 'FIG'">
            <t:value-of select="concat ('$B?^(B ', substring (attribute::clause, 4))"/>
          </t:when>
          <t:when test="substring (attribute::clause, 1, 1) = '4'">
            <t:value-of select="concat ('$BDj5A(B ',
                                        substring (attribute::clause, 2, 3))"/>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="' $BBh(B '"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 5)"/>
              <t:with-param name="suffix" select="' $BCJMn(B'"/>
            </t:call-template>
          </t:when>
          <t:when test="substring (attribute::clause, 2, 1) = '.'">
            <t:value-of select="concat
                                  ('$BImB0=q(B ',
                                   translate (substring (attribute::clause, 1, 1),
                                              'abcdefghijklmnopqrstuvwxyz',
                                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                                   '.')"/>
            <t:call-template name="clause-number">
              <t:with-param name="number"
                            select="substring (attribute::clause, 3, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 4, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 5, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 6, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="' $BBh(B '"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 7, 1)"/>
              <t:with-param name="suffix" select="' $BCJMn(B'"/>
            </t:call-template>
          </t:when>
          <t:otherwise>
            <t:call-template name="clause-number">
              <t:with-param name="number"
                            select="substring (attribute::clause, 1, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 2, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 3, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="'.'"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 4, 1)"/>
            </t:call-template>
            <t:call-template name="clause-number">
              <t:with-param name="prefix" select="' $BBh(B '"/>
              <t:with-param name="number"
                            select="substring (attribute::clause, 5, 1)"/>
              <t:with-param name="suffix" select="' $BCJMn(B'"/>
            </t:call-template>
          </t:otherwise>
        </t:choose>
      </t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template name="clause-number">
    <t:param name="prefix"/>
    <t:param name="number"/>
    <t:param name="suffix"/>
    <t:choose>
      <t:when test="$number = '0'"/>
      <t:when test="$number = 'a'">
        <t:value-of select="concat ($prefix, '10', $suffix)"/>
      </t:when>
      <t:when test="$number = 'b'">
        <t:value-of select="concat ($prefix, '11', $suffix)"/>
      </t:when>
      <t:when test="$number = 'c'">
        <t:value-of select="concat ($prefix, '12', $suffix)"/>
      </t:when>
      <t:when test="$number = 'd'">
        <t:value-of select="concat ($prefix, '13', $suffix)"/>
      </t:when>
      <t:when test="$number = 'e'">
        <t:value-of select="concat ($prefix, '14', $suffix)"/>
      </t:when>
      <t:otherwise>
        <t:value-of select="concat ($prefix, $number, $suffix)"/>
      </t:otherwise>
    </t:choose>
  </t:template>
  
  <t:template name="attribute-default">
    <h2:di>
      <h2:dt>$B4{Dj%0%m!<%V@_7W(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="ancestor::psmodule and attribute::default = 'default'">
            $B4^$^$l$k(B
          </t:when>
          <t:when test="ancestor::psmodule">$B4^$^$l$J$$(B</t:when>
          <t:otherwise>$B4^$^$l$k(B</t:otherwise>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attribute-dependon">
    <h2:di>
      <h2:dt>$B0MB8%b%8%e!<%k(B</h2:dt>
      <h2:dd>
        <h2:choose>
          <h2:when test="attribute::dependon">
            <h2:value-of select="attribute::dependon"/>
          </h2:when>
          <h2:otherwise>$B$J$7(B</h2:otherwise>
        </h2:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attribute-prune">
    <h2:di>
      <h2:dt>$B%0%m!<%V@_7W$+$i$N=|5n(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="attribute::prune = 'nprune'">$B;R6!$r@\LZ(B</t:when>
          <t:otherwise>$BFbMFLZ$r4"<h$j(B</t:otherwise>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attribute-mayadd">
    <h2:di>
      <h2:dt>$B<+F0DI2C(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="attribute::mayadd = 'mayadd'">$B2DG=(B</t:when>
          <t:otherwise>$BIT2DG=(B</t:otherwise>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attribute-vrfytype">
    <h2:di>
      <h2:dt>$B8!>Z7?(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="attribute::mayadd = 'derived'">$BB>$NFC@-$+$iGI@8(B</t:when>
          <t:when test="attribute::mayadd = 'optional'">$B<+F04IM}2DG=(B</t:when>
          <t:otherwise>$B$=$NB>(B</t:otherwise>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template match="attribute::conprop">
    <h2:di>
      <h2:dt>$BFbMFFC@-(B</h2:dt>
      <h2:dd href="#{generate-id (ancestor::propset/descendant::propdef[string (attribute::rcsnm) = string (current ())])}"><t:value-of select="self::node ()"/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template match="attribute::despprop">
    <h2:di>
      <h2:dt>$B%G!<%?J,N%;RFC@-(B</h2:dt>
      <h2:dd href="#{generate-id (ancestor::propset/descendant::propdef[attribute::rcsnm = self::node ()])}"><t:value-of select="self::node ()"/></h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="attribute-cn">
    <h2:di>
      <h2:dt>$B5i(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="attribute::cn = '#ALL' or attribute::cn = '#all'">
            <t:value-of select="($B$9$Y$F(B)"/>
          </t:when>
          <t:when test="attribute::cn">
            <t:for-each select="ancestor::propset/descendant::classdef[attribute::rcsnm = string (current ()/attribute::cn)]">
              <t:attribute name="href">
                <t:value-of select="concat ('#', generate-id (self::node ()))"/>
              </t:attribute>
              <t:call-template name="fullname"/>
            </t:for-each>
          </t:when>
          <t:otherwise>
            <t:for-each select="ancestor::classdef">
              <t:attribute name="href">
                <t:value-of select="concat ('#', generate-id (self::node ()))"/>
              </t:attribute>
              <t:call-template name="fullname"/>
            </t:for-each>
          </t:otherwise>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template match="attribute::datatype">
    <h2:di>
      <h2:dt>$B%G!<%?7?(B</h2:dt>
      <h2:dd>
        <t:choose>
          <t:when test="self::node () = 'node'">
            <t:value-of select="'$B@aE@(B'"/>
            <t:call-template name="noderel"/>
          </t:when>
          <t:when test="self::node () = 'nodelist'">
            <t:value-of select="'$B@aE@JB$S(B'"/>
            <t:call-template name="noderel"/>
          </t:when>
          <t:when test="self::node () = 'nmndlist'">
            <t:value-of select="'$BL>A0IU$-@aE@JB$S(B'"/>
            <t:call-template name="noderel"/>
            ($BL>A0FC@-(B: <t:value-of select="parent::node ()/attribute::acnmprop"/>)
          </t:when>
          <t:when test="self::node () = 'enum'">
            <h2:ul>
              <h2:label>$BNs5s(B</h2:label>
              <h2:li>
                <t:apply-templates select="parent::node ()/child::enumdef"/>
              </h2:li>
            </h2:ul>
          </t:when>
          <t:when test="self::node () = 'char'">$BJ8;z(B</t:when>
          <t:when test="self::node () = 'string'">
            $BJ8;zNs(B
            <t:apply-templates select="parent::node ()/attribute::strnorm"/>
          </t:when>
          <t:when test="self::node () = 'strlist'">
            $BJ8;zNsJB$S(B
            <t:apply-templates select="parent::node ()/attribute::strnorm"/>
          </t:when>
          <t:when test="self::node () = 'integer'">$B@0?t(B</t:when>
          <t:when test="self::node () = 'intlist'">$B@0?tJB$S(B</t:when>
          <t:when test="self::node () = 'boolean'">$B??56CM(B</t:when>
          <t:when test="self::node () = 'compname'">$BItIJL>(B</t:when>
          <t:when test="self::node () = 'cnmlist'">$BItIJL>JB$S(B</t:when>
        </t:choose>
      </h2:dd>
    </h2:di>
  </t:template>
  
  <t:template name="noderel">
    (<t:choose>
      <t:when test="parent::node ()/attribute::noderel = 'subnode'">$BItJ,@aE@(B</t:when>
      <t:when test="parent::node ()/attribute::noderel = 'irefnode'">$BFbIt;2>H@aE@(B</t:when>
      <t:when test="parent::node ()/attribute::noderel = 'urefnode'">$BHs@)8B;2>H@aE@(B</t:when>
    </t:choose><t:choose>
      <t:when test="parent::node ()/attribute::ac">
        <t:value-of select="' '"/>
        <t:value-of select="parent::node ()/attribute::ac"/>
      </t:when>
    </t:choose>)
  </t:template>
  
  <t:template match="attribute::strnorm">
    ($B@55,2=(B: <t:for-each select="ancestor::propset/descendant::normdef[attribute::rcsnm = string (current ())]"><h2:a href="#{generate-id (self::node ())}"><t:call-template name="fullname"/></h2:a></t:for-each>)
  </t:template>
</t:stylesheet>
