<?xml version="1.0" encoding="iso-2022-jp"?>
<ss:stylesheet version="1.0"
    xmlns:ss="http://www.w3.org/1999/XSL/Transform"
    xmlns:p="http://suika.fam.cx/~wakaba/archive/2005/2/htp#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:t="http://suika.fam.cx/~wakaba/archive/2005/2/m17n#"
    xmlns:h="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml">
  <ss:variable name="documentLanguage" select="'en'"/>
  <ss:variable name="documentTitle" select="'Known HTML Problems'"/>
  <ss:variable name="tocSectionTitle" select="'Table of Contents'"/>
  <ss:template match="p:doc">
    <html lang="{$documentLanguage}" xml:lang="{$documentLanguage}">
      <head>
        <title><ss:value-of select="$documentTitle"/></title>
        <link rel="stylesheet" href="/www/style/html/xhtml"
            media="all"/>
        <link rel="copyright" href="#LICENSE"/>
      </head>
      <body>
        <h1><ss:value-of select="$documentTitle"/></h1>
        
        <dl lang="en" xml:lang="en">
        <dt>This Version</dt>
          <dd>&lt;<a href="/www/2005/htp/htp">http://suika.fam.cx/www/2005/htp/htp</a>&gt;
            [<a href="/www/2005/htp/htp.xml"
                type="text/xml">XML</a> /
            <a href="/www/2005/htp/htp.ja.html"
                hreflang="ja" type="text/html">English, HTML</a> /
            <a href="/www/2005/htp/htp.ja.html"
                hreflang="ja" type="text/html">
              <span lang="ja" xml:lang="ja">$BF|K\8l(B</span>, HTML</a>]</dd>
        <dt>Last Modified</dt>
          <dd><ss:value-of select="/child::p:doc/@lastModified.RCS"/></dd>
        </dl>
        
        <ss:call-template name="abstractSection"/>
        
        <div class="section" id="STATUS" lang="en" xml:lang="en">
          <h2>Status of This Document</h2>
          <p>Work in progress.</p>
        </div>
        
        <div class="section" id="TOC">
          <h2><ss:value-of select="$tocSectionTitle"/></h2>
          <ul><ss:apply-templates mode="toc"/></ul>
        </div>
        
        <ss:apply-templates/>
        
        <ss:call-template name="aboutSection"/>
      </body>
    </html>
  </ss:template>
  
  <ss:template name="abstractSection">
        <div class="section" id="ABSTRACT">
          <h2>Abstract</h2>
          
          <p>This document summarizes problems on HTML specifications
             and gives pointers to discussions on them.</p>
        </div>
  </ss:template>

  <ss:template name="aboutSection">
    <div class="section">
      <h2>About</h2>
      
      <p>Comments are welcomed; please write on the wiki: 
         <cite><a href="/~wakaba/-temp/wiki/wiki?Known%20HTML%20Problems">SuikaWiki : Known HTML Problems</a></cite>
         (in Japanese or English).</p>
      
      <ss:call-template name="licenseSection"/>
    </div>
  </ss:template>

  <ss:template name="licenseSection">
    <div class="section" id="LICENSE" xml:lang="en" lang="en">
      <h3>License</h3>
      
      <p>Copyright &#xA9; 2005.</p>
      
            <p>Permission is granted to copy, distribute and / or modify 
               this document under the terms of the 
               <a href="http://www.gnu.org/copyleft/fdl.html">GNU Free 
               Documentation License, Version 1.2</a> or any later version 
               published by the <a href="http://www.fsf.org/">Free Software 
               Foundation</a>; with no Invariant Sections, no Front$B!>(BCover 
               Texts, and no Back$B!>(BCover Texts.  A copy of the license is 
               available at <code class="uri">&lt;<a href="http://www.gnu.org/copyleft/fdl.html">http://www.gnu.org/copyleft/fdl.html</a>&gt;</code>.</p>
      </div>
  </ss:template>

  <ss:template match="p:entry" mode="toc">
    <li><a href="#{@xml:id}"><ss:apply-templates select="child::dc:title"/></a></li>
  </ss:template>
  
  <ss:variable name="sSemantics" select="'Semantics'"/>
  <ss:variable name="sSyntax" select="'Syntax'"/>
  <ss:variable name="sEdit" select="'Editorial'"/>
  <ss:variable name="lSpec" select="'Specifications'"/>
  <ss:variable name="lArticle" select="'Articles'"/>
  <ss:variable name="lRelated" select="'Related Issues'"/>
  
  <ss:template match="p:entry">
    <div class="section {@s}" id="{@xml:id}">
      <h2><ss:apply-templates select="child::dc:title"/>
        [<ss:choose>
         <ss:when test="@s = 'semantics'"><ss:value-of select="$sSemantics"/></ss:when>
         <ss:when test="@s = 'syntax'"><ss:value-of select="$sSyntax"/></ss:when>
         <ss:when test="@s = 'edit'"><ss:value-of select="$sEdit"/></ss:when>
         <ss:otherwise><ss:value-of select="@s"/></ss:otherwise>
         </ss:choose>]</h2>
      <ss:apply-templates select="child::p:problem |
                                  child::p:situation |
                                  child::p:current"/>
      <ss:if test="child::p:H2 |
                   child::p:H3 |
                   child::p:H4 |
                   child::p:M12N |
                   child::p:UG |
                   child::p:XH2">
        <div class="section">
          <h3><ss:value-of select="$lSpec"/></h3>
          <ul><ss:apply-templates select="child::p:H2 |
                                          child::p:H3 |
                                          child::p:H4 |
                                          child::p:M12N |
                                          child::p:UG |
                                          child::p:XH2"/></ul>
        </div>
      </ss:if>
      <ss:if test="child::p:ref |
                   child::p:SW">
        <div class="section">
          <h3><ss:value-of select="$lArticle"/></h3>
          <ul><ss:apply-templates select="child::p:ref |
                                          child::p:SW"/></ul>
        </div>
      </ss:if>
      <ss:if test="child::p:related">
        <div class="section">
          <h3><ss:value-of select="$lRelated"/></h3>
          <ul><ss:apply-templates select="child::p:related"/></ul>
        </div>
      </ss:if>
    </div>
  </ss:template>
  
  <ss:variable name="iProblem" select="'Problem'"/>
  <ss:variable name="iSituation" select="'Situation'"/>
  <ss:variable name="iCurrent" select="'Current Practice'"/>
  
  <ss:template match="p:problem">
    <div class="section problem">
      <h3><ss:value-of select="$iProblem"/></h3>
      <ul><ss:apply-templates/></ul>
    </div>
  </ss:template>
  <ss:template match="p:situation">
    <div class="section situation">
      <h3><ss:value-of select="$iSituation"/></h3>
      <ul><ss:apply-templates/></ul>
    </div>
  </ss:template>
  <ss:template match="p:current">
    <div class="section current">
      <h3><ss:value-of select="$iCurrent"/></h3>
      <ul><ss:apply-templates/></ul>
    </div>
  </ss:template>
  
  <ss:template match="p:i">
    <li><ss:apply-templates/></li>
  </ss:template>
  
  <ss:variable name="inEnglish" select="''"/>
  <ss:variable name="inJapanese" select="' (in Japanese)'"/>
  
  <ss:template match="p:H2">
    <li><a href="urn:ietf:rfc:1866" lang="en" xml:lang="en"
           hreflang="en">HTML 2.0 (RFC 1866)</a>
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:H3">
    <li><a href="urn:ietf:id:draft-ietf-html-specv3-00"
           lang="en" xml:lang="en" hreflang="en">HTML 3.0 Internet Draft</a>
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:H4">
    <li lang="en" xml:lang="en"><a href="http://www.w3.org/TR/html4/{@section}"
                                   hreflang="en">HTML 4</a> 
          [<a href="http://www.w3.org/TR/REC-html40-971218/{@section}"
              hreflang="en">4.0 <abbr title="First Edition">FE</abbr></a> /
           <a href="http://www.w3.org/TR/1998/REC-html40-19980424/{@section}"
              hreflang="en">4.0 <abbr title="Second Edition">SE</abbr></a> / 
           <a href="http://www.w3.org/TR/1999/REC-html401-19991224/{@section}"
              hreflang="en">4.01</a>]
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:M12N">
    <li><a href="http://www.w3.org/TR/xhtml-modularization/{@section}"
           lang="en" xml:lang="en" hreflang="en">XHTML m12n 1.0</a> 
          [<a href="http://www.w3.org/TR/2001/REC-xhtml-modularization-20010410/{@section}"
              lang="en" xml:lang="en" hreflang="en"><abbr title="First Edition">FE</abbr></a>]
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:UG">
    <li><a href="http://purl.org/NET/ISO+IEC.15445/Users-Guide.html#{@fragment}"
           lang="en" xml:lang="en" hreflang="en">User's Guide to ISO-HTML</a>
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:XH2">
    <li><a href="http://www.w3.org/TR/xhtml2/{@section}"
           lang="en" xml:lang="en" hreflang="en">XHTML 2.0</a>
        <ss:value-of select="$inEnglish"/></li>
  </ss:template>
  
  <ss:template match="p:ref">
    <li>
      <a href="{@uri}">
        <ss:if test="@hrefLang">
          <ss:attribute name="hreflang"><ss:value-of select="@hrefLang"/></ss:attribute>
        </ss:if>
        <ss:apply-templates select="child::dc:title"/>
      </a>
      <ss:if test="child::dc:author">
        <ss:text> by </ss:text>
        <ss:apply-templates select="child::dc:author"/>
      </ss:if>
      <ss:choose>
      <ss:when test="@hrefLang = 'ja'"><ss:value-of select="$inJapanese"/></ss:when>
      <ss:when test="@hrefLang = 'en'"><ss:value-of select="$inEnglish"/></ss:when>
      <ss:when test="@hrefLang"> (In <ss:value-of select="@hrefLang"/>)</ss:when>
      </ss:choose>
    </li>
  </ss:template>
  
  <ss:template match="p:SW">
    <ss:choose>
    <ss:when test="@anchor">
      <li><a href="/~wakaba/-temp/wiki/wiki?_charset_=utf-8;mypage={@page}#anchor-{@anchor}" hreflang="ja">SuikaWiki : <ss:value-of select="@page"/>
        >><ss:value-of select="@anchor"/></a>
        <ss:value-of select="$inJapanese"/></li>
    </ss:when>
    <ss:otherwise>
      <li><a href="/~wakaba/-temp/wiki/wiki?_charset_=utf-8;mypage={@page}" hreflang="ja">SuikaWiki : <ss:value-of select="@page"/></a>
        <ss:value-of select="$inJapanese"/></li>
    </ss:otherwise>
    </ss:choose>
  </ss:template>
  
  <ss:template match="p:related">
    <li><a href="#{@ref}"><ss:apply-templates select="/child::p:doc/child::p:entry[string (@xml:id) = string (current ()/@ref)]/dc:title"/></a></li>
  </ss:template>
  
  <ss:template match="t:Alt">
    <ss:choose>
    <ss:when test="child::*[lang ('en')]">
      <ss:apply-templates select="child::*[lang ('en')]"/>
    </ss:when>
    <ss:otherwise>
      <ss:apply-templates select="(child::*)[position () = 1]"/>
    </ss:otherwise>
    </ss:choose>
  </ss:template>
  
  <ss:template match="p:fileName">
    <code class="file">
      <ss:if test="@xml:lang">
        <ss:attribute name="lang"><ss:value-of select="@xml:lang"/></ss:attribute>
        <ss:attribute name="xml:lang"><ss:value-of select="@xml:lang"/></ss:attribute>
      </ss:if>
      <ss:apply-templates/>
    </code>
  </ss:template>
  
  <ss:template match="p:he">
    <code class="HTMLe" lang="en" xml:lang="en"><ss:apply-templates/></code>
  </ss:template>
  
  <ss:template match="p:ha">
    <code class="HTMLa" lang="en" xml:lang="en"><ss:apply-templates/></code>
  </ss:template>
  
  <ss:template match="p:xhe">
    <code class="XMLe" lang="en" xml:lang="en"><ss:apply-templates/></code>
  </ss:template>
  
  <ss:template match="p:xha">
    <code class="XMLa" lang="en" xml:lang="en"><ss:apply-templates/></code>
  </ss:template>
  
  <ss:template match="h:code">
    <code>
      <ss:if test="@xml:lang">
        <ss:attribute name="lang"><ss:value-of select="@xml:lang"/></ss:attribute>
        <ss:attribute name="xml:lang"><ss:value-of select="@xml:lang"/></ss:attribute>
      </ss:if>
      <ss:apply-templates/>
    </code>
  </ss:template>
  
  <ss:template match="h:var">
    <var>
      <ss:if test="@xml:lang">
        <ss:attribute name="lang"><ss:value-of select="@xml:lang"/></ss:attribute>
        <ss:attribute name="xml:lang"><ss:value-of select="@xml:lang"/></ss:attribute>
      </ss:if>
      <ss:apply-templates/>
    </var>
  </ss:template>
  
  <ss:template match="h:q">
    <q>
      <ss:if test="@cite">
        <ss:attribute name="cite"><ss:value-of select="@cite"/></ss:attribute>
      </ss:if>
      <ss:if test="@xml:lang">
        <ss:attribute name="lang"><ss:value-of select="@xml:lang"/></ss:attribute>
        <ss:attribute name="xml:lang"><ss:value-of select="@xml:lang"/></ss:attribute>
      </ss:if>
      <ss:apply-templates/>
    </q>
  </ss:template>
  
  <ss:template match="h:em">
    <em><ss:apply-templates/></em>
  </ss:template>
</ss:stylesheet>
