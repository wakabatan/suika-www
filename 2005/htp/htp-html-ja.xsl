<?xml version="1.0" encoding="iso-2022-jp"?>
<ss:stylesheet version="1.0"
    xmlns:ss="http://www.w3.org/1999/XSL/Transform"
    xmlns:p="http://suika.fam.cx/~wakaba/archive/2005/2/htp#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:t="http://suika.fam.cx/~wakaba/archive/2005/2/m17n#"
    xmlns:h="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml">
  <ss:import href="htp-html-en.xsl"/>
  <ss:variable name="documentLanguage" select="'ja'"/>
  <ss:variable name="documentTitle" select="'HTML $B$N4{CN$NLdBjE@(B'"/>
  <ss:variable name="tocSectionTitle" select="'$BL\<!(B'"/>
  <ss:variable name="sSemantics" select="'$B0UL#(B'"/>
  <ss:variable name="sSyntax" select="'$B9=J8(B'"/>
  <ss:variable name="sEdit" select="'$BJT=8(B'"/>
  <ss:variable name="lSpec" select="'$B;EMM=q(B'"/>
  <ss:variable name="lArticle" select="'$B5-;v(B'"/>
  <ss:variable name="lRelated" select="'$B4X78$9$kLdBj(B'"/>
  <ss:variable name="iProblem" select="'$BLdBj(B'"/>
  <ss:variable name="iSituation" select="'$B>u67(B'"/>
  <ss:variable name="iCurrent" select="'$B8=>u(B'"/>
  <ss:variable name="inEnglish" select="' ($B1Q8l(B)'"/>
  <ss:variable name="inJapanese" select="''"/>
  
  <ss:template name="abstractSection">
    <div class="section" id="ABSTRACT">
      <h2>$B35MW(B</h2>
      
      <p>$B$3$NJ8=q$G$O(B HTML $B$NLdBjE@$H$=$l$K$D$$$F$N5DO@$,$"$kJ8=q$r$^$H$a$F$$$^$9!#(B</p>
    </div>
  </ss:template>
  
  <ss:template name="aboutSection">
    <div class="section">
      <h2>$B$3$NJ8=q$K$D$$$F(B</h2>
      
      <p>$B$3$NJ8=q$K4X$9$k$40U8+Ey$r$*4s$;2<$5$$!#(B
         <cite><a href="http://suika.fam.cx/~wakaba/-temp/wiki/wiki?Known%20HTML%20Problems">SuikaWiki : 
         <span lang="en" xml:lang="en">Known HTML Problems</span></a></cite>
         $B$K(B ($BF|K\8l$^$?$O1Q8l$G(B) $B=q$$$F2<$5$$!#(B</p>
      
      <ss:call-template name="licenseSection"/>
    </div>
  </ss:template>
  
  
  <ss:template match="t:Alt">
    <ss:choose>
    <ss:when test="child::*[lang ('ja')]">
      <ss:apply-templates select="child::*[lang ('ja')]"/>
    </ss:when>
    <ss:otherwise>
      <ss:apply-templates select="(child::*)[position () = 1]"/>
    </ss:otherwise>
    </ss:choose>
  </ss:template>

</ss:stylesheet>
