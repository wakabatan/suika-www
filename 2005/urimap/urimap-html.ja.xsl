<!DOCTYPE t:stylesheet [
  <!ENTITY prop "http://suika.fam.cx/admin/assign/urn-20050519#DT-">
]>
<t:stylesheet version="1.0"
        xmlns:t="http://www.w3.org/1999/XSL/Transform"
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:prop="http://suika.fam.cx/admin/assign/urn-20050519#DT-"
        xmlns:map="http://suika.fam.cx/~wakaba/2005/6/uri-table#"
        xmlns:replace="http://suika.fam.cx/~wakaba/archive/2005/6/replace#">

  <t:template match="/">
    <t:processing-instruction name="xml-stylesheet">
      href="<t:call-template name="document-style-sheet-uri"/>" media="all"
    </t:processing-instruction>
    <t:variable name="style2"><t:call-template
        name="document-style-sheet-add-uri"/></t:variable>
    <t:if test="string-length($style2)">
      <t:processing-instruction name="xml-stylesheet">
        href="<t:value-of select="$style2"/>" media="all"
      </t:processing-instruction>
    </t:if>
    <html xml:lang="ja" lang="ja">
      <t:attribute name="class">
        <t:call-template name="document-class"/>
      </t:attribute>
      <head>
        <title><t:call-template name="document-title"/></title>
        <link rel="stylesheet" media="all">
          <t:attribute name="href">
            <t:call-template name="document-style-sheet-uri"/>
          </t:attribute>
        </link>
        <t:if test="string-length($style2)">
          <link rel="stylesheet" media="all">
            <t:attribute name="href">
              <t:value-of select="$style2"/>
            </t:attribute>
          </link>
        </t:if>
      </head>
      <body>
        <t:call-template name="document-header"/>
        <t:apply-templates select="child::rdf:RDF"/>
        <t:call-template name="document-footer"/>
      </body>
    </html>
  </t:template>

  <t:template name="document-title">
    URI $B<LA|I=(B
  </t:template>

  <t:template name="document-class"/>

  <t:template name="document-style-sheet-uri">/www/style/html/xhtml</t:template>

  <t:template name="document-style-sheet-add-uri"/>

  <t:template name="document-header">
        <h1><t:call-template name="uri"/> $B<LA|I=(B</h1>
  </t:template>

  <t:template name="document-footer">
        <div class="footer">
          <div class="navigation">[<a href="/" rel="home">/</a>
             <t:value-of select="' '"/>
             <a href="/search/" rel="search">$B8!:w(B</a>]</div>
        </div>
  </t:template>

  <t:template match="child::rdf:RDF">
    <t:apply-templates select="child::*"/>
  </t:template>

  <t:template match="child::map:Resource">
    <div class="section">
      <h2>
        <t:call-template name="uri"/>
        <t:value-of select="' '"/>
        <t:call-template name="uriref">
          <t:with-param name="uri" select="attribute::rdf:about"/>
        </t:call-template>
      </h2>
      <t:call-template name="entry-props">
        <t:with-param name="entry" select="self::node()"/>
        <t:with-param name="entry-type" select="'Resource'"/>
      </t:call-template>
    </div>
  </t:template>

  <t:template match="child::map:ResourceGroup">
    <div class="section">
      <h2>
        <t:call-template name="text-uri-group"/>
        <t:call-template name="uriref">
          <t:with-param name="uri" select="attribute::rdf:about"/>
        </t:call-template>
      </h2>
      <t:call-template name="entry-props">
        <t:with-param name="entry" select="self::node()"/>
        <t:with-param name="entry-type" select="'ResourceGroup'"/>
      </t:call-template>
    </div>
  </t:template>

  <t:template match="/child::rdf:RDF/child::*" priority="-100">
    <div class="section">
      <h2>
        <t:call-template name="uriref">
          <t:with-param name="uri"
            select="concat(namespace-uri(), local-name())"/>
        </t:call-template>
      </h2>
    </div>
      <t:call-template name="entry-props">
        <t:with-param name="entry" select="self::node()"/>
        <t:with-param name="entry-type" select="'Other'"/>
      </t:call-template>
  </t:template>

  <t:template name="entry-props">
    <t:param name="entry"/>
    <t:param name="entry-type"/>
    <dl>
      <t:call-template name="entry-map-props">
        <t:with-param name="entry" select="self::node()"/>
        <t:with-param name="entry-type" select="string($entry-type)"/>
      </t:call-template>
      <t:apply-templates
        select="$entry/child::*[not(self::prop:*) and not (self::map:*)]"/>
    </dl>
  </t:template>

  <t:template name="entry-map-props">
    <t:param name="entry"/>
    <t:param name="entry-type"/>
    <dt>$B85$N(B <t:call-template name="uri"/></dt>
    <dd>
      <t:call-template name="uriref">
        <t:with-param name="uri" select="$entry/attribute::rdf:about"/>
      </t:call-template>
      <t:if test="$entry-type = 'ResourceGroup'">
        $B$G;O$^$k$b$N(B
      </t:if>
    </dd>
    <t:apply-templates select="$entry/child::prop:* |
                               $entry/child::map:*"/>
  </t:template>

  <t:template match="child::prop:PRIMARY-RESOLVED-URI">
      <dt>$B0l<!2r7h(B <t:call-template name="uri"/></dt>
      <dd><t:call-template name="uritemplate">
            <t:with-param name="template" select="self::node()"/>
          </t:call-template></dd>
  </t:template>

  <t:template match="child::prop:SECONDARY-RESOLVED-URI">
      <dt>$BFs<!2r7h(B <t:call-template name="uri"/></dt>
      <dd><t:call-template name="uritemplate">
            <t:with-param name="template" select="self::node()"/>
          </t:call-template></dd>
  </t:template>

  <t:template match="child::map:see">
      <dt>$B30It<LA|I=;2>H(B</dt>
      <dd>
        <t:call-template name="uriref">
          <t:with-param name="uri" select="attribute::rdf:resource"/>
        </t:call-template>
      </dd>
  </t:template>

  <t:template match="/child::rdf:RDF/child::*/child::*" priority="-100">
    <dt>
      <t:call-template name="uriref">
        <t:with-param name="uri" select="concat(namespace-uri(), local-name())"/>
      </t:call-template>
    </dt>
    <dd><t:apply-templates/></dd>
  </t:template>

  <t:template name="uri">
    <abbr lang="en" xml:lang="en"
      title="Uniform Resource Identifiers">URI</abbr>
  </t:template>

  <t:template name="uriref">
    <t:param name="uri"/>
    <code class="URI">&lt;<a>
      <t:attribute name="href"><t:value-of select="$uri"/></t:attribute>
      <t:value-of select="$uri"/>
    </a>&gt;</code>
  </t:template>

  <t:template name="mailref">
    <t:param name="mail"/>
    <code class="mail">&lt;<a>
      <t:attribute name="href">mailto:<t:value-of select="$mail"/></t:attribute>
      <t:value-of select="$mail"/>
    </a>&gt;</code>
  </t:template>

  <t:template name="uritemplate">
    <t:param name="template"/>
    <t:choose>
    <t:when test="descendant::replace:*">
      <code class="URI">
        <t:value-of select="'&lt;'"/>
        <t:apply-templates select="child::node()"/>
        <t:value-of select="'&gt;'"/>
      </code>
    </t:when>
    <t:otherwise>
      <t:call-template name="uriref">
        <t:with-param name="uri" select="string($template)"/>
      </t:call-template>
    </t:otherwise>
    </t:choose>
  </t:template>

  <t:template match="child::replace:*">
    <var>
      <t:value-of select="'('"/>
      <t:value-of select="local-name()"/>
      <t:apply-templates select="attribute::*"/>
      <t:if test="child::node()">
        <t:value-of select="': '"/>
        <t:apply-templates select="child::node()"/>
      </t:if>
      <t:value-of select="')'"/>
    </var>
  </t:template>

  <t:template match="child::replace:*/attribute::*">
    <t:value-of select="' (@'"/>
    <code class="XMLa">
      <t:if test="namespace-uri()">
        <t:value-of select="'{'"/>
        <t:value-of select="namespace-uri()"/>
        <t:value-of select="'}:'"/>
      </t:if>
      <t:value-of select="local-name()"/>
    </code>
    <t:value-of select="'='"/>
    <t:value-of select="string(self::node())"/>
    <t:value-of select="')'"/>
  </t:template>

  <t:template name="text-uri-group">
        <t:call-template name="uri"/>
        <t:value-of select="' $B72(B '"/>
  </t:template>
</t:stylesheet>
