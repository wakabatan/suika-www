window.onload = function () {
  document.getElementById ('SUBMIT').onclick = function () {
    var uri_re = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/;
    var original_uri = document.getElementById ('INPUT-URI').value;
    if (original_uri.match (uri_re)) {
      setValue ('RESULT-URI-SCHEME', RegExp.$2, RegExp.$1);
      setValue ('RESULT-URI-AUTHORITY', RegExp.$4, RegExp.$3);
      setValue ('RESULT-URI-PATH', RegExp.$5, true);
      setValue ('RESULT-URI-QUERY', RegExp.$7, RegExp.$6);
      setValue ('RESULT-URI-FRAGMENT', RegExp.$9, RegExp.$8);
    }
  };
};

function setValue (elID, val, isNonNull) {
  var el = document.getElementById (elID);
  if (!isNonNull) {
    el.innerHTML = '$B!](B';
  } else {
    el.innerHTML = '';
    var code = document.createElement ('code');
    code.setAttribute ('class', 'URI');
    code.appendChild (document.createTextNode (val));
    el.appendChild (code);
  }
}
