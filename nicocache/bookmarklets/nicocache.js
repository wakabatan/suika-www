self.NicoCache = {};

SAMI.Class.addClassMethods (NicoCache, {
  withDirList: function (code) {
    var self = this;

    if (self.dirList) {
      code (self.dirList);
    }

    new SAMI.XHR ('http://www.nicovideo.jp/cache/dirlist.json', function () {
      self.dirList = new SAMI.List (this.getJSON ());
      code (self.dirList);
    }).get();
  } // withDirList
}); // NicoCache class methods
 
NicoCache.Video = new SAMI.Class (function (id) {
  this.id = id;
}, {
  callAjaxAPI: function (path, query, okCode, ngCode) {
    new SAMI.XHR ('http://www.nicovideo.jp/cache/ajax_' + path + '?' + query, function () {
      var res = this.getText ();
      if (/^OK /.test (res)) {
        okCode (res.substring (3));
      } else {
        ngCode (res);
      }
    }, function () {
      ngCode ('');
    }).get ();
  }, // callAjaxAPI
  callAjaxAPIWithId: function (path, okCode, ngCode) {
    var self = this;
    self.callAjaxAPI (path, encodeURIComponent (self.id), function (res) {
      okCode (res);
    }, function () {
      self.callAjaxAPI (path, encodeURIComponent (self.id) + 'low', function (res) {
        okCode (res);
      }, function () {
        ngCode ();
      });
    });
  }, // callAjaxAPIWithId

  withProperty: function (name, path, code) {
    var self = this;
    
    if (self[name] !== undefined) {
      code (self[name]);
      return;
    }

    self.callAjaxAPIWithId (path, function (res) {
      self[name] = res;
      code (self[name]);
    }, function () {
      self[name] = code ();
    });
  }, // withProperty

  withTitle: function (code) {
    var self = this;

    return this.withProperty ('title', 'titlefromcache', function (res) {
      if (res == null) {
        code (self.id);
        return self.id;
      } else {
        code (res);
      }
    });
  }, // withTitle
  withDirectory: function (code) {
    var self = this;
    
    return this.withProperty ('directory', 'directory', function (res) {
      if (res == null) {
        code (null);
        return null;
      } else {
        code (res.replace (/^cache\//, ''));
      }
    });
  }, // withDirectory

  setDirectory: function (value, code) {
    this.callAjaxAPI ('move', encodeURIComponent (this.id) + '-' + encodeURIComponent (value), function (res) {
      code (false, 'OK ' + res);
    }, function (res) {
      code (false, res);
    });
    this.callAjaxAPI ('move', encodeURIComponent (this.id) + 'low-' + encodeURIComponent (value), function (res) {
      code (true, 'OK ' + res);
    }, function (res) {
      code (true, res);
    });
  } // setDirectory
}); // Video

NicoCache.Page = new SAMI.Class (function (doc) {
  this.doc = doc;
}, {
  getId: function () {
    if (this.id) return this.id;

    var links = this.doc.links;
    var linksL = links.length;
    for (var i = 0; i < linksL; i++) {
      var link = links[i];
      var m = link.href.match (/\/cache\/([a-z]{2}[0-9]+)\/movie$/);
      if (m) {
        this.id = m[1];
        return this.id;
      }
    }

    return null;
  } // getId
}); // Page

/* ***** BEGIN LICENSE BLOCK *****
 * Copyright 2009 Wakaba <w@suika.fam.cx>.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the same terms as Perl itself.
 *
 * Alternatively, the contents of this file may be used 
 * under the following terms (the "MPL/GPL/LGPL"), 
 * in which case the provisions of the MPL/GPL/LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of the MPL/GPL/LGPL, and not to allow others to
 * use your version of this file under the terms of the Perl, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the MPL/GPL/LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the Perl or the MPL/GPL/LGPL.
 *
 * "MPL/GPL/LGPL":
 *
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <http://www.mozilla.org/MPL/>
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is NicoCache-bookmarklets code.
 *
 * The Initial Developer of the Original Code is Wakaba.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Wakaba <w@suika.fam.cx>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the LGPL or the GPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
