javascript:

if (!self.SAMI) self.SAMI = {};
if (!SAMI.onLoadFunctions) SAMI.onLoadFunctions = [];

SAMI.onLoadFunctions.push (function () {
  SAMI.Script.loadScripts (new SAMI.List ([
    'http://suika.fam.cx/www/nicocache/bookmarklets/nicocache.js?' + new Date ()
  ]), function () {
    var ControlBox = new SAMI.Class (function () {
      var article = document.createElement ('article');
      article.innerHTML = '<p style=float:right><button type=button class=nc-close>$B!_(B</button><p><output class=nc-video-title></output><p><output class=nc-original-cache-directory></output><p style=text-align:right>-> <select class=nc-cache-directories style="min-width: 10em"></select> <button type=button class=nc-cache-directory-change>Change</button><p class=nc-high-status><p class=nc-low-status>';
      article.setAttribute ('style', 'position: fixed; display: block; top: 2em; left: 1em; width: 20em; background-color: white; color: black; border: outset gray 4px; z-index: 10000');
      document.body.appendChild (article);
      this.element = article;

      this.getElements ('nc-close').list[0].onclick = function () {
        article.parentNode.removeChild (article);
      }; // onclick
    }, {
      getElements: function (key) {
        return SAMI.Node.getElementsByClassName (this.element, key);
      }, // getElements

      setVideoTitle: function (v) {
        this.getElements ('nc-video-title').list[0].textContent = v;
      }, // setVideoTitle
      setOriginalCacheDirectory: function (v) {
        this.getElements ('nc-original-cache-directory').list[0].textContent = v;
      }, // setOriginalCacheDirectory
      setCacheDirectories: function (l) {
        var select = this.getElements ('nc-cache-directories').list[0];
        select.innerHTML = '';
        l.forEach (function (dir) {
          var opt = document.createElement ('option');
          opt.textContent = dir;
          if (dir == '') {
            opt.label = '(root)';
          }
          select.appendChild (opt);
        });

        var self = this;
        this.getElements ('nc-cache-directory-change').list[0].onclick = function () {
          self.changeDirectory ();
        };
      }, // setCacheDirectories

      setHighStatus: function (v) {
        this.getElements ('nc-high-status').list[0].textContent = 'High: ' + v;
      }, // setHighStatus
      setLowStatus: function (v) {
        this.getElements ('nc-low-status').list[0].textContent = 'Low: ' + v;
      }, // setLowStatus

      clearHighStatus: function () {
        this.setHighStatus ('');
      }, // clearHighStatus
      clearLowStatus: function () {
        this.setLowStatus ('');
      }, // setLowStatus

      changeDirectory: function () {
        var newValue = this.getElements ('nc-cache-directories').list[0].value;
        this.onchangedirectory (newValue);
      } // changeDirectory
    });

    var box = new ControlBox;

    var p = new NicoCache.Page (document);
    var id = p.getId ();
    if (id) {
      var v = new NicoCache.Video (id);
      v.withTitle (function (title) {
        box.setVideoTitle (title);
      });
      v.withDirectory (function (dir) {
        box.setOriginalCacheDirectory (dir);
      });

      box.onchangedirectory = function (d) {
        box.clearLowStatus ();
        box.clearHighStatus ();
        v.setDirectory (d, function (isLow, res) {
          if (isLow) {
            box.setLowStatus (res);
          } else {
            box.setHighStatus (res);
          }
        });
      }; // onchangedirectory
    }
    NicoCache.withDirList (function (dirs) {
      box.setCacheDirectories (dirs);
    });
  });
});

var script = document.createElement ('script');
script.src = 'http://suika.fam.cx/www/js/sami/script/sami-core.js';
document.body.appendChild (script);

void (0);

